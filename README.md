# @plotfunbot

A [Telegram](https://telegram.org/) bot for Math functions graphs and fun.

## Check it live

https://t.me/plotfunbot

## What you need

* Java 11 JDK
* MySql 8 / MariaDB Server

## How to run

* Clone the repo
* `cd` to directory
* Register a new Telegram bot
* Create a MySql database and optionally a MySql user with proper rights to the database
* Create your `config.properties` file based on the example
* `cd app && ln -s ../config.properties config.properties && cd ..`

### In local environment

* `./gradlew :app:run --no-daemon`
* Or run the `App` class in your favorite IDE

### In production environment

*  `./gradlew clean shadowJar`
* `java -jar app/build/libs/app-all.jar`

## Configure the bot

You will need to register a Telegram bot, which is associated with your Telegram account.

To register a Telegram bot use [BotFather](https://t.me/BotFather). Set the bot name, description and get the `API Token`

Previous to the first run, you have to create a `config.properties` configuration file. Make a copy of the provided `config.properties.EXAMPLE` and edit it as follows:

* Database settings:
  * Set the MySql database name in `FLYWAY_URL` and `DB_DATABASE` 
  * Set the MySql user in `FLYWAY_USER` and `DB_USERNAME`
  * Set the MySql password in `FLYWAY_PASSWORD` and `DB_PASSWORD`
  * If you get trouble working with buttons with emoji, try setting `ENCODE_EMOJI=1`
* Telegram bot settings:
  * Set the bot name (finished in `bot`, `Bot`, etc) in `BOT_NAME`
  * Set the bot API Token in `TELEGRAM_API_KEY`
* Security settings:
  * Set a first-time administrator PIN in `MAGIC_CODE`. Example: `8888`
* Logging settings:
  * Set `SESSIONS_TO_LOG` to `*` to log all interactions. Or set it to a comma-separated list of Telegram chat ID's
  (Note: chat ID's appear `[between_brackets]` in all logged interactions)
* Other settings:
  * Set `FALLBACK_RESPONSE` to the text to be used as a default response to the user when there are not matching interactions defined in code
  * If you modify the bot to use the `GeolocService`, you will need a [LocationIQ](https://locationiq.com) API Key. Set it in `LOCATION_IQ_API_KEY`

## Run the bot

The first time you run the bot, it will prompt all users for the first-time administrator PIN. The first user that enters it correctly will become an `ADMIN` and will have access to the `/admin` command.

Once you enter the first-time administrator PIN, you can tap on `/start` to access the user-level interactions.

To complete the configuration, use the `/admin` command to assign some users (for example, you) the `OPERATOR` role for both the available scopes. See class `Scopes`.

Note that after a user has been assigned an administrative privilege, they should restart the bot with `/start` to get the permissions applied.

## Additional information

Check the [Talks framework](https://gitlab.com/gllona/talks) website.

## Author

Developed by [Gorka Llona](http://desarrolladores.logicos.org/gorka).
