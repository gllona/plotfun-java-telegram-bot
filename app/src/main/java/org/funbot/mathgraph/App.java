package org.funbot.mathgraph;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.api.AppApi;
import org.logicas.librerias.talks.api.ModelApi;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;

public class App implements AppApi {

    private static Logger logger = LogManager.getLogger(App.class);

    private TalksConfiguration talksConfig;
    private ModelApi model;

    public static void main(String[] args) {
        new App(args);
    }

    private App(String[] args) {
        logger.info("Starting app....");
        AppConfig.getInstance().init();
        talksConfig = buildTalksConfiguration();
        model = startModel();
        talksConfig.getInputAdaptersManager().startInputAdapters();
        registerShutDownHook();
        logger.info("App started.");
    }

    private TalksConfiguration buildTalksConfiguration() {
        return new TalksConfiguration() {
            @Override
            public AppApi getApp() {
                return App.this;
            }
            @Override
            public ModelApi getModel() {
                return Model.getInstance(this);
            }
            @Override
            public List<TalkType> getTalkTypes() {
                return Talks.getAllTalkTypes();
            }
        };
    }

    private ModelApi startModel() {
        logger.info("Starting Model....");
        ModelApi model = talksConfig.getModel();
        logger.info("Model started.");
        return model;
    }

    private void registerShutDownHook() {
        Runtime.getRuntime().addShutdownHook(
            new Thread(this::shutdown)
        );
    }

    @Override
    public void shutdown() {
        logger.info("Stopping app....");
        talksConfig.getInputAdaptersManager().stopInputAdapters();
        model.shutdown();
        logger.info("App stopped.");
    }
}
