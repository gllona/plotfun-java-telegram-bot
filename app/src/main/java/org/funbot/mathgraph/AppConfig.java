package org.funbot.mathgraph;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import javax.management.AttributeNotFoundException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.flywaydb.core.Flyway;
import org.logicas.librerias.talks.config.Constants;

public class AppConfig {

    private static Logger logger = LogManager.getLogger(AppConfig.class);
    private static AppConfig instance = getInstance();

    private Properties properties;

    private AppConfig() throws IOException, AttributeNotFoundException {
        readProperties();
        checkProperties();
    }

    public static synchronized AppConfig getInstance() {
        if (instance == null) {
            try {
                instance = new AppConfig();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return instance;
    }

    public void init() {
        migrateDatabase();
    }

    private static void migrateDatabase() {
        logger.info("Migrating database....");
        Flyway flyway = Flyway.configure().dataSource(
            AppSetting.get(AppSetting.FLYWAY_URL),
            AppSetting.get(AppSetting.FLYWAY_USER),
            AppSetting.get(AppSetting.FLYWAY_PASSWORD)
        ).load();
        flyway.migrate();
        logger.info("Database migrated (if you get an SSLException please ignore it).");
    }

    private void readProperties() throws IOException {
        FileInputStream fis = new FileInputStream(Constants.PROPERTIES_FILE);
        properties = new Properties();
        properties.load(fis);
    }

    private void checkProperties() throws AttributeNotFoundException {
        logger.info("Checking all properties are set....");
        for (AppSetting setting : AppSetting.values()) {
            String value = properties.getProperty(setting.name());
            if (value == null) {
                logger.error(String.format("Property [%s] is not set!!", setting.name()));
                throw new AttributeNotFoundException(setting.name());
            }
        }
    }

    public String getString(AppSetting setting) {
        return properties.getProperty(setting.name());
    }
}
