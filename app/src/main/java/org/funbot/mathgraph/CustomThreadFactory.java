package org.funbot.mathgraph;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class CustomThreadFactory {

    private static final Logger logger = LogManager.getLogger(CustomThreadFactory.class);
    private static CustomThreadFactory instance = null;

    private final Map<String, Map<Thread, Object>> threadMap = Collections.synchronizedMap(new WeakHashMap<>());

    public static synchronized CustomThreadFactory getInstance() {
        if (instance == null) {
            instance = new CustomThreadFactory();
        }
        return instance;
    }

    public ThreadFactory create(String id) {
        return new ThreadFactory() {
            private final Map<Thread, Object> threads = Collections.synchronizedMap(new WeakHashMap<>());
            private final AtomicInteger threadCount = new AtomicInteger(0);
            @Override
            public Thread newThread(Runnable r) {
                int counter = threadCount.incrementAndGet();
                Thread t = new Thread(r, id + " thread  " + counter) {
                    @Override
                    public void interrupt() {
                        logger.info("Interrupting " + getName() + "....");
                        super.interrupt();
                    }
                };
                threads.put(t, "14");
                threadMap.putIfAbsent(id, threads);
                return t;
            }
        };
    }

    public void interruptThreads(String threadsId) {
        Map<Thread, Object> threads = threadMap.get(threadsId);
        for (Thread thread : threads.keySet()) {
            thread.interrupt();
        }
        threadMap.remove(threadsId);
    }
}
