package org.funbot.mathgraph;

import com.pengrad.telegrambot.model.Update;
import org.funbot.mathgraph.admin.TelemetryTracker;
import org.funbot.mathgraph.stores.ShowcaseCache;
import org.logicas.librerias.talks.api.InputAdapterApi;
import org.logicas.librerias.talks.api.ModelApi;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Model implements ModelApi {

    private static ModelApi instance;

    private TalksConfiguration talksConfig;
    private TelemetryTracker telemetryTracker = TelemetryTracker.getInstance();
    private ShowcaseCache showcaseCache;

    public static synchronized ModelApi getInstance(TalksConfiguration talksConfig) {
        if (instance == null) {
            instance = new Model(talksConfig);
        }
        return instance;
    }

    private Model(TalksConfiguration talksConfig) {
        this.talksConfig = talksConfig;
        this.showcaseCache = ShowcaseCache.getInstance(talksConfig.getDao());
        talksConfig.getTalkManager();
    }

    @Override
    public void shutdown() {
        showcaseCache.shutdown();
        talksConfig.getTalkManager().shutDown();
    }

    @Override
    public Optional<TalkResponse> receiveMessage(InputAdapterApi inputAdapter, Update update) {
        TalkRequest talkRequest = new TalkRequest(
            inputAdapter,
            update
        );

        telemetryTracker.beforeDispatch(talkRequest);

        Optional<TalkResponse> talkResponse = talksConfig.getTalkManager().dispatch(talkRequest);

        telemetryTracker.afterDispatch(talkRequest, talkResponse.orElse(null));

        return talkResponse;
    }

    @Override
    public void beforeReceiveMessages(InputAdapterApi inputAdapter, List<Update> updates) {
        telemetryTracker.beforeDispatch(
            updates.stream().map(u -> new TalkRequest(inputAdapter, u)).collect(Collectors.toList())
        );
    }

    @Override
    public void afterReceiveMessages(InputAdapterApi inputAdapter, List<Update> updates) {
        telemetryTracker.afterDispatch(
            updates.stream().map(u -> new TalkRequest(inputAdapter, u)).collect(Collectors.toList())
        );
    }

    // OTHER API CALLS GO HERE (e.g. REST CALLS)
}
