package org.funbot.mathgraph;

import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

public abstract class PlaceMainTalk extends Talk {

    protected PlaceMainTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
    }

    public abstract TalkResponse menu(TalkRequest request);
}
