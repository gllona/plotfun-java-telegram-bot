package org.funbot.mathgraph;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.funbot.mathgraph.admin.AdminTalk;
import org.funbot.mathgraph.admin.AuthTalk;
import org.funbot.mathgraph.admin.TelemetryTalk;
import org.funbot.mathgraph.communicator.CommunicatorTalk;
import org.funbot.mathgraph.fun.FunTalk;
import org.funbot.mathgraph.fun.MyStoreTalk;
import org.funbot.mathgraph.fun.ShowcaseTalk;
import org.funbot.mathgraph.intro.WelcomeTalk;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.engine.Talk;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Getter
public enum Talks implements TalkType {

    // Important: All enum elements should have the same name and capitalization as the class name
    FunTalk(FunTalk.class),
    MyStoreTalk(MyStoreTalk.class),
    ShowcaseTalk(ShowcaseTalk.class),
    WelcomeTalk(WelcomeTalk.class),
    AdminTalk(AdminTalk.class),
    AuthTalk(AuthTalk.class),
    TelemetryTalk(TelemetryTalk.class),
    CommunicatorTalk(CommunicatorTalk.class);

    private Class<? extends Talk> talkClass;

    public static List<TalkType> getAllTalkTypes() {
        return Arrays.stream(values())
            .map(t -> (TalkType) t)
            .collect(Collectors.toList());
    }

    @Override
    public Class<? extends Talk> getTalkClass() {
        return talkClass;
    }

    @Override
    public String getName() {
        return talkClass.getSimpleName();
    }
}
