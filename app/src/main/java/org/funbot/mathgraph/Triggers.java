package org.funbot.mathgraph;

public class Triggers {

    public static final String COMMAND = "\\/.+";
    public static final String NOT_A_COMMAND = "[^/].*";

    public static final String FOUR_DIGITS = "[0-9]{4}";
    public static final String POSITIVE_INTEGER = "[0-9]+";
    public static final String FLOATING_POINT = "[+-]?([0-9]+(\\.[0-9]+)?|\\.[0-9]+)";

    public static final String RGB_COLOR = "#([0-9a-fA-F]){6}";

    private static final String HASHID = "[A-Za-z0-9]+";

    public static final String BOX_ID_COMMAND = "/box_" + HASHID;
}
