package org.funbot.mathgraph.admin;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.funbot.mathgraph.Place;
import org.funbot.mathgraph.Triggers;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

public class AuthService {

    private static AuthService instance;

    private TalksConfiguration talksConfig;
    private UserRepository userRepository;

    public static synchronized AuthService getInstance(TalksConfiguration talksConfig) {
        if (instance == null) {
            instance = new AuthService(talksConfig);
        }
        return instance;
    }

    public static AuthService getInstance() {
        if (instance == null) {
            throw new RuntimeException("Auth not initialized");
        }
        return instance;
    }

    private AuthService(TalksConfiguration talksConfig) {
        this.talksConfig = talksConfig;
        this.userRepository = UserRepository.getInstance(talksConfig.getDao());
    }

    public Optional<TalkResponse> firstTimeRoute(Long chatId) {
        if (userRepository.hasAdmins()) {
            return Optional.empty();
        }

        return Optional.of(
            TalkResponse.ofText(chatId, "Welcome")
                .withRegex("admin.AuthTalk::firstTimeCode", Triggers.FOUR_DIGITS)
        );
    }

    public void firstTimeMagicCodeGot(TalkRequest request) {
        User user = new User();
        user.setTgChatId(request.getChatId());
        user.setTgUsername(request.getMessage().chat().username());
        user.setRole(Role.ADMIN);
        userRepository.save(user);
    }

    public boolean userCanAuthorize(long chatId) {
        return userHasRole(chatId, Role.ADMIN, Optional.empty());
    }

    public boolean userHasRole(long chatId, Role role, Optional<String> scope) {
        List<User> usersWithRoles = userRepository.readByTgChatId(chatId);

        Optional<User> userRole = usersWithRoles.stream()
            .filter(u -> u.getRole() == role)
            .filter(u -> ! scope.isPresent() || scope.get().equals(u.getScope()))
            .findFirst();

        return userRole.isPresent();
    }

    public List<User> findAllPrivilegedUsers() {
        return userRepository.readAllPrivileged().stream()
            .filter(u -> u.getRole() != Role.USER)
            .collect(Collectors.toList());
    }

    public void save(String tgUsername, Role role, String scope) {
        List<User> existingUsers = userRepository.readByTgUsernameAndRoleAndScope(tgUsername, role, scope);
        if (existingUsers.size() != 0) {
            return;
        }

        User user = new User();
        user.setTgUsername(tgUsername);
        user.setRole(role);
        user.setScope(scope);
        userRepository.save(user);
    }

    public void delete(String tgUsername, Role role, String scope) {
        List<User> existingUsers = userRepository.readByTgUsernameAndRoleAndScope(tgUsername, role, scope);
        for (User user : existingUsers) {
            userRepository.delete(user);
        }
    }

    public void fillInUsers(TalkRequest request) {
        if (request.getMessage().chat().username() == null) {
            return;
        }
        List<User> userRoles = userRepository.readByField("tg_username", request.getMessage().chat().username(), false, false);
        for (User userRole : userRoles) {
            userRole.setTgChatId(request.getChatId());
            userRepository.save(userRole);
        }
    }

    public User findUser(long tgChatId) {
        return userRepository.readByTgChatId(tgChatId).stream().findFirst().orElse(null);
    }

    public void saveUser(TalkRequest request) {
        Long chatId = request.getChatId();
        String username = request.getMessage().chat().username();
        User user = findUser(chatId);

        if (user == null) {
            createUser(chatId, username);
        } else {
            updateUser(user, username);
        }
    }

    private void createUser(long tgChatId, String tgUsername) {
        User user = new User();
        user.setRole(Role.USER);
        user.setScope(Place.FUN.name());
        user.setTgChatId(tgChatId);
        user.setTgUsername(tgUsername);
        userRepository.save(user);
    }

    private void updateUser(User user, String tgUsername) {
        user.setTgUsername(tgUsername);
        userRepository.save(user);
    }

    public boolean userCanCommunicateWithOtherUsers(long chatId) {
        return userRepository.isPrivileged(chatId);
    }
}
