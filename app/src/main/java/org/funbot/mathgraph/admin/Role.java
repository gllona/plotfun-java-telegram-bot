package org.funbot.mathgraph.admin;

import static java.util.Arrays.asList;

import java.util.List;

public enum Role {

    ADMIN,
    OPERATOR,
    USER;

    public static Role from(String text) {
        if (text == null) {
            return null;
        }
        try {
            return Role.valueOf(text);
        } catch (Exception e) {
            return null;
        }
    }

    public static List<Role> privileged() {
        return asList(ADMIN, OPERATOR);
    }
}
