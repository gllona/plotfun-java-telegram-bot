package org.funbot.mathgraph.admin;

import org.funbot.mathgraph.Triggers;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class TelemetryTalk extends Talk {

    private TelemetryTracker telemetryTracker = TelemetryTracker.getInstance();

    public TelemetryTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("FunTalk");
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.empty();
    }

    public TalkResponse showStats(TalkRequest request) {
        String stats = buildStats();

        return TalkResponse.ofText(
            request.getChatId(),
            stats
        ).withButton("admin.TelemetryTalk::showStats", "Again")
         .withButton("admin.TelemetryTalk::quantile", "Quantile")
         .withButton("admin.TelemetryTalk::methods", "Per method")
         .withButton("admin.TelemetryTalk::reset", "Reset")
         .withButton("admin.AdminTalk::admin", "Back");
    }

    private String buildStats() {
        String totalCount = "Total count = " + telemetryTracker.getTotalCount();
        String perRequest = "Per request:\n" +
            "  min = " + telemetryTracker.getMin() + "\n" +
            "  MAX = " + telemetryTracker.getMax() + "\n" +
            "  median = " + telemetryTracker.getMedian() + "\n" +
            "  quantile(0.25) = " + telemetryTracker.getQuantile(0.25f) + "\n" +
            "  quantile(0.75) = " + telemetryTracker.getQuantile(0.75f) + "\n" +
            "  quantile(0.90) = " + telemetryTracker.getQuantile(0.90f);
        String perBunch = "Per bunch\n" +
            "  min = " + telemetryTracker.getBunchMin() + "\n" +
            "  MAX = " + telemetryTracker.getBunchMax() + "\n" +
            "  median = " + telemetryTracker.getBunchMedian() + "\n" +
            "  quantile(0.25) = " + telemetryTracker.getBunchQuantile(0.25f) + "\n" +
            "  quantile(0.75) = " + telemetryTracker.getBunchQuantile(0.75f) + "\n" +
            "  quantile(0.90) = " + telemetryTracker.getBunchQuantile(0.90f);

        return totalCount + "\n\n" + perRequest + "\n\n" + perBunch;
    }

    public TalkResponse quantile(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "Enter quantile factor (0.0 - 1.0):"
        ).withRegex("admin.TelemetryTalk::doQuantile", Triggers.FLOATING_POINT);
    }

    public TalkResponse doQuantile(TalkRequest request) {
        String quantileFactorAsString = request.getMessage().text();
        float quantileFactor = Float.parseFloat(quantileFactorAsString);

        int quantile = telemetryTracker.getQuantile(quantileFactor);
        int bunchQuantile = telemetryTracker.getBunchQuantile(quantileFactor);

        return TalkResponse.ofText(
            request.getChatId(),
            "Quantile(" + quantileFactor + ") =\n" +
                "  per request --> " + quantile + "\n" +
                "  per bunch --> " + bunchQuantile
        ).withButton("admin.TelemetryTalk::showStats", "Continue");
    }

    public TalkResponse methods(TalkRequest request) {
        List<String> trackedMethods = telemetryTracker.getTrackedMethods();

        String statsStr = trackedMethods.stream()
            .map(method -> {
                Map<Float, Integer> stats = telemetryTracker.getStatsPerMethod(method);
                return "* " + method + " -->\n" +
                    "  " +
                    stats.entrySet().stream()
                        .sorted(Map.Entry.comparingByKey())
                        .map(e -> e.getKey() + "=" + e.getValue())
                        .collect(Collectors.joining(" "));
            })
            .collect(Collectors.joining("\n"));

        return TalkResponse.ofText(
            request.getChatId(),
            "Tracked methods:\n" +
                "\n" +
                statsStr
        ).withButton("admin.TelemetryTalk::showStats", "Continue");
    }

    public TalkResponse reset(TalkRequest request) {
        telemetryTracker.reset();

        return TalkResponse.ofText(
            request.getChatId(),
            "Telemetry reset done."
        ).withButton("admin.TelemetryTalk::showStats", "Continue");
    }
}
