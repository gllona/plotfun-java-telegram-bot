package org.funbot.mathgraph.admin;

import com.dynatrace.dynahist.Histogram;
import com.dynatrace.dynahist.layout.Layout;
import com.dynatrace.dynahist.layout.LogQuadraticLayout;
import lombok.Getter;
import lombok.Setter;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class TelemetryTracker {

    @Getter
    @Setter
    private static class TelemetryEntry {
        private TalkResponse response;
        private Long beforeDispatchInstantInMillis;
        private Long afterDispatchInstantInMillis;
        private Long beforeDispatchBunchInstantInMillis;
        private Long afterDispatchBunchInstantInMillis;
        long dispatchingTimeInMillis() {
            return afterDispatchInstantInMillis - beforeDispatchInstantInMillis;
        }
        long bunchDispatchingTimeInMillis() {
            return afterDispatchBunchInstantInMillis - beforeDispatchBunchInstantInMillis;
        }
    }

    private static TelemetryTracker instance = null;

    private Map<TalkRequest, TelemetryEntry> entries = new ConcurrentHashMap<>();
    private Histogram singleEntryHistogram = null;
    private Histogram entryBunchHistogram = null;
    private Map<String, Histogram> perMethodHistograms = new HashMap<>();
    private Layout histogramLayout = LogQuadraticLayout.create(10, 0.05, 0, 30 * 60);

    public static synchronized TelemetryTracker getInstance() {
        if (instance == null) {
            instance = new TelemetryTracker();
        }
        return instance;
    }

    private TelemetryTracker() {
        initializeHistograms();
    }

    private void initializeHistograms() {
        reset();
    }

    public synchronized void reset() {
        singleEntryHistogram = Histogram.createDynamic(histogramLayout);
        entryBunchHistogram = Histogram.createDynamic(histogramLayout);
        perMethodHistograms.clear();
    }

    public synchronized void beforeDispatch(TalkRequest talkRequest) {
        long now = System.currentTimeMillis();
        TelemetryEntry entry = entries.get(talkRequest);
        entry.setBeforeDispatchInstantInMillis(now);
    }

    public synchronized void afterDispatch(TalkRequest talkRequest, TalkResponse talkResponse) {
        long now = System.currentTimeMillis();
        TelemetryEntry entry = entries.get(talkRequest);
        entry.setResponse(talkResponse);
        entry.setAfterDispatchInstantInMillis(now);
        trackSingleEntry(entry);
    }

    // received talkRequests objects are not the same as for the methods above for the same updates
    public synchronized void beforeDispatch(List<TalkRequest> talkRequests) {
        long now = System.currentTimeMillis();
        for (TalkRequest request : talkRequests) {
            TelemetryEntry entry = new TelemetryEntry();
            entry.setBeforeDispatchBunchInstantInMillis(now);
            entries.put(request, entry); // map value is left for future usage
        }
    }

    // received talkRequests objects are not the same as for the methods above for the same updates
    public synchronized void afterDispatch(List<TalkRequest> talkRequests) {
        long now = System.currentTimeMillis();
        for (TalkRequest request : talkRequests) {
            TelemetryEntry entry = entries.get(request);
            entry.setAfterDispatchBunchInstantInMillis(now);
            trackBunchEntry(entry);
            entries.remove(request);
        }
    }
    private void trackSingleEntry(TelemetryEntry entry) {
        singleEntryHistogram.addValue(
            entry.dispatchingTimeInMillis()
        );
        TalkResponse.Telemetry telemetry = entry.getResponse().getTelemetry();
        if (telemetry != null) {
            getPerMethodHistogram(telemetry.getClassAndMethod())
                .addValue(telemetry.getDurationInMillis());
        }
    }

    private Histogram getPerMethodHistogram(String classAndMethod) {
        return perMethodHistograms.computeIfAbsent(
            classAndMethod,
            cm -> Histogram.createDynamic(histogramLayout)
        );
    }

    private void trackBunchEntry(TelemetryEntry entry) {
        entryBunchHistogram.addValue(
            entry.bunchDispatchingTimeInMillis()
        );
    }

    public synchronized long getTotalCount() {
        return singleEntryHistogram.getTotalCount();
    }

    public synchronized int getMin() {
        return new Double(singleEntryHistogram.getMin()).intValue();
    }

    public synchronized int getMax() {
        return new Double(singleEntryHistogram.getMax()).intValue();
    }

    public synchronized int getBunchMin() {
        return new Double(entryBunchHistogram.getMin()).intValue();
    }

    public synchronized int getBunchMax() {
        return new Double(entryBunchHistogram.getMax()).intValue();
    }

    public synchronized int getMedian() {
        return new Double(singleEntryHistogram.getQuantile(0.5)).intValue();
    }

    public synchronized int getBunchMedian() {
        return new Double(entryBunchHistogram.getQuantile(0.5)).intValue();
    }

    public synchronized int getQuantile(float quantileFactor) {
        return new Double(singleEntryHistogram.getQuantile(quantileFactor)).intValue();
    }

    public synchronized int getBunchQuantile(float quantileFactor) {
        return new Double(entryBunchHistogram.getQuantile(quantileFactor)).intValue();
    }

    public List<String> getTrackedMethods() {
        return perMethodHistograms.keySet().stream()
            .sorted()
            .collect(Collectors.toList());
    }

    public Map<Float, Integer> getStatsPerMethod(String method) {
        Histogram histogram = perMethodHistograms.get(method);
        float[] quantiles = new float[]{ 0.1f, 0.25f, 0.5f, 0.75f, 0.9f };
        Map<Float, Integer> values = new HashMap<>();

        for (float quantile : quantiles) {
            values.put(quantile, new Double(histogram.getQuantile(quantile)).intValue());
        }

        return values;
    }
}
