package org.funbot.mathgraph.admin;

import java.util.HashMap;
import java.util.Map;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.logicas.librerias.copersistance.entities.SingleKeyEntity;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class User extends SingleKeyEntity {

    public static final String FIELD_ID = "id";
    public static final String FIELD_CHAT_ID = "tg_chat_id";
    public static final String FIELD_USERNAME = "tg_username";
    public static final String FIELD_EMAIL = "email";
    public static final String FIELD_ROLE = "role";
    public static final String FIELD_SCOPE = "scope";
    public static final String FIELD_VISITS = "visits";

    public static final String[] FIELDS = {
        FIELD_ID,
        FIELD_CHAT_ID,
        FIELD_USERNAME,
        FIELD_EMAIL,
        FIELD_ROLE,
        FIELD_SCOPE,
        FIELD_VISITS
    };

    private static final String[] KEYS = {
        FIELD_ID
    };

    private Long id;
    private Long tgChatId;
    private String tgUsername;
    private String email;
    private String role;
    private String scope;
    private Integer visits;

    @Override
    public Map<String, Object> parameters() {
        Map<String, Object> params = new HashMap<>();

        conditionalAddToParams(id, FIELD_ID, params);
        conditionalAddToParams(tgChatId, FIELD_CHAT_ID, params);
        conditionalAddToParams(tgUsername, FIELD_USERNAME, params);
        conditionalAddToParams(email, FIELD_EMAIL, params);
        conditionalAddToParams(role, FIELD_ROLE, params);
        conditionalAddToParams(scope, FIELD_SCOPE, params);
        conditionalAddToParams(visits, FIELD_VISITS, params);

        return params;
    }

    @Override
    public String[] getKeyColumns() {
        return KEYS;
    }

    public Role getRole() {
        return Role.valueOf(role);
    }

    public void setRole(Role role) {
        this.role = role.name();
    }

    public String prettyToString() {
        return "Telegram username = " + (tgUsername == null ? "(not defined)" : "@" + tgUsername) +
            "\n  email = " + (email == null ? "(not defined)" : email) +
            "\n  role = " + role +
            "\n  scope = " + (scope == null ? "(all)" : scope);
    }
}
