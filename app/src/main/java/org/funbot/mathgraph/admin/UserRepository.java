package org.funbot.mathgraph.admin;

import java.util.List;
import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.repositories.QueryBuilder;
import org.logicas.librerias.copersistance.repositories.SingleKeyEntityRepository;

public class UserRepository extends SingleKeyEntityRepository<User> {

    private static UserRepository instance;

    private Dao dao;

    private UserRepository(Dao dao) {
        this.dao = dao;
    }

    public static synchronized UserRepository getInstance(Dao dao) {
        if (instance == null) {
            instance = new UserRepository(dao);
        }
        return instance;
    }

    private static final String TABLE = "users";

    @Override
    protected Dao dao() {
        return dao;
    }

    @Override
    public String tableName() {
        return TABLE;
    }

    @Override
    protected String[] columnNames() {
        return User.FIELDS;
    }

    @Override
    public String idFieldName() {
        return User.FIELD_ID;
    }

    public boolean hasAdmins() {
        return countAll("role = 'ADMIN'") > 0;
    }

    public List<User> readByTgChatId(long chatId) {
        return readByField("tg_chat_id", chatId);
    }

    public List<User> readAllPrivileged() {
        return readByCondition("role <> 'USER'");
    }

    public boolean isPrivileged(long chatId) {
        return readAllPrivileged().stream()
            .anyMatch(u -> u.getTgChatId() == chatId);
    }

    public List<User> readByTgUsernameAndRoleAndScope(String tgUsername, Role role, String scope) {
        String condition = scope != null ?
            "tg_username = '" + QueryBuilder.escapeSql(tgUsername) + "'" +
                " AND role = '" + role.name() + "'" +
                " AND scope = '" + scope + "'" :
            "tg_username = '" + QueryBuilder.escapeSql(tgUsername) + "'" +
                " AND role = '" + role.name() + "'" +
                " AND scope IS NULL" ;
        return readByCondition(condition);
    }

    public List<User> readByRoleAndScope(Role role, String scope) {
        String condition = scope != null ?
            "role = '" + role.name() + "' AND scope = '" + scope + "'" :
            "role = '" + role.name() + "' AND scope IS NULL" ;
        return readByCondition(condition);
    }
}
