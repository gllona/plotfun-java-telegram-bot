package org.funbot.mathgraph.communicator;

import static java.util.Collections.singletonList;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.funbot.mathgraph.admin.User;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;
import org.funbot.mathgraph.Place;
import org.funbot.mathgraph.admin.Role;
import org.funbot.mathgraph.admin.UserRepository;

public class CommunicatorService {

    static final int TG_MESSAGE_LINE_MAX_CHARS = 24;   // 30 in 1080p phones

    private static CommunicatorService instance;

    private final TalksConfiguration talksConfig;
    private final TextWrapperService textWrapperService;
    private final UserRepository userRepository;

    public static synchronized CommunicatorService getInstance(TalksConfiguration talksConfig) {
        if (instance == null) {
            instance = new CommunicatorService(talksConfig);
        }
        return instance;
    }

    public CommunicatorService(TalksConfiguration talksConfig) {
        this.talksConfig = talksConfig;
        this.textWrapperService = TextWrapperService.getInstance(TG_MESSAGE_LINE_MAX_CHARS);
        this.userRepository = UserRepository.getInstance(talksConfig.getDao());
    }

    public void sendToOperatorsUnboxed(TalkRequest request, Place place,
                                       String header, String message, String footer, String postFooter
    ) {
        sendToOperators(request, place, Optional.empty(), header, message, footer, postFooter, false);
    }

    public void sendToOperatorsBoxed(TalkRequest request, Place place,
                                     String header, String message, String footer, String postFooter
    ) {
        sendToOperatorsBoxed(request, place, Optional.empty(), header, message, footer, postFooter);
    }

    public void sendToOperatorsBoxed(TalkRequest request, Place place,
                                     Optional<Long> exceptThisOperatorChatId,
                                     String header, String message, String footer, String postFooter
    ) {
        sendToOperators(request, place, exceptThisOperatorChatId, header, message, footer, postFooter, true);
    }

    private void sendToOperators(TalkRequest request, Place place,
                                 Optional<Long> exceptThisOperatorChatId,
                                 String header, String message, String footer, String postFooter,
                                 boolean boxed
    ) {
        List<User> allOperators = userRepository.readByRoleAndScope(Role.OPERATOR, place == null ? null : place.name());
        List<Long> operators = allOperators.stream()
            .filter(operator -> ! exceptThisOperatorChatId.isPresent() || ! operator.getTgChatId().equals(exceptThisOperatorChatId.get()))
            .map(User::getTgChatId)
            .collect(Collectors.toList());

        sendTo(request, operators, header, message, footer, postFooter, boxed);
    }

    public void sendToUser(TalkRequest request, long targetChatId,
                           String header, String message, String footer, String postFooter,
                           boolean boxed
    ) {
        sendTo(request, singletonList(targetChatId), header, message, footer, postFooter, boxed);
    }

    private void sendTo(TalkRequest request, List<Long> chatIds,
                        String header, String message, String footer, String postFooter,
                        boolean boxed
    ) {
        String fullMessage = Stream.of(
            Optional.ofNullable(header),
            Optional.of(""),
            Optional.ofNullable(message),
            Optional.of(""),
            Optional.ofNullable(footer)
        ).filter(Optional::isPresent)
         .map(Optional::get)
         .collect(Collectors.joining("\n"));
        List<String> fullMessageParts = textWrapperService.buildMessageParts(fullMessage, boxed);

        for (long chatId : chatIds) {
            for (String part : fullMessageParts) {
                request.getInputAdapter().sendResponse(
                    TalkResponse.ofHtml(
                        chatId,
                        part
                    ).withPreserveHandlers()
                );
            }
            if (postFooter != null) {
                request.getInputAdapter().sendResponse(
                    TalkResponse.ofText(
                        chatId,
                        postFooter
                    ).withPreserveHandlers()
                );
            }
        }
    }

    public void sendToAdmins(TalkRequest request, String message) {
        List<User> admins = userRepository.readByRoleAndScope(Role.ADMIN, null);
        List<String> boxedMessageParts = textWrapperService.buildMessageParts(message, true);

        for (User admin : admins) {
            for (String part : boxedMessageParts) {
                request.getInputAdapter().sendResponse(
                    TalkResponse.ofHtml(
                        admin.getTgChatId(),
                        part
                    )
                );
            }
        }
    }
}
