package org.funbot.mathgraph.fun;

import org.funbot.mathgraph.PlaceMainTalk;
import org.funbot.mathgraph.Triggers;
import org.funbot.mathgraph.admin.AuthService;
import org.funbot.mathgraph.geoloc.GeolocService;
import org.funbot.mathgraph.plot.*;
import org.funbot.mathgraph.stores.ShowcaseService;
import org.funbot.mathgraph.stores.StoreService;
import org.funbot.mathgraph.stores.StoresUtils;
import org.logicas.librerias.talks.api.FileSystemApi;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.InputTrigger;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;
import org.logicas.librerias.talks.filesystem.FileSystem;

import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static org.funbot.mathgraph.stores.StoresUtils.getInstance;

public class FunTalk extends PlaceMainTalk {

    public static final int GRAPH_SIZE = Plotter.PLOT_SIZE;

    public static final String FUN_TYPE = "FUN_TYPE";
    public static final String FUN_Y_BY_X = "FUN_Y_BY_X";
    public static final String FUN_X_BY_P = "FUN_X_BY_P";
    public static final String FUN_Y_BY_P = "FUN_Y_BY_P";

    public static final String IV_MIN = "IV_MIN";
    public static final String IV_MAX = "IV_MAX";

    public static final String LINE_COLOR = "LINE_COLOR";
    public static final String LINE_THICKNESS = "LINE_THICKNESS";
    public static final String LINE_STYLE = "LINE_STYLE";
    public static final String BACKGROUND_COLOR = "BACKGROUND_COLOR";
    public static final String DISPLAY_AXES = "DISPLAY_AXES";

    public static final String BOX_ID = "BOX_ID";
    public static final String FUN_NAME = "FUN_NAME";
    public static final String FUN_TAGS = "FUN_TAGS";

    public static final String DISPLAY_FUN_MODE = "DISPLAY_MODE";
    public static final String DISPLAY_FUN_MODE_EDIT = "EDIT";
    public static final String DISPLAY_FUN_MODE_LIKE = "LIKE";

    private static final String FUN_GENERATOR = "FUN_GENERATOR";
    private static final String FUN_DESCRIPTION = "FUN_DESCRIPTION";
    private static final String FUN_DV_IV = "FUN_DV_IV";

    private static final String BOX_ATTRIBUTES_EDITING = "ATTRIBUTES_EDITION";
    private static final String BOX_ATTRIBUTES_EDITING_FUN_MENU = "FUN_MENU";
    private static final String BOX_ATTRIBUTES_EDITING_AFTER_SHOWN = "AFTER_SHOWN";

    private final AuthService authService = AuthService.getInstance(talksConfig);
    private final GeolocService geolocService = GeolocService.getInstance(talksConfig);
    private final StoreService storeService = StoreService.getInstance(talksConfig.getDao());
    private final ShowcaseService showcaseService = ShowcaseService.getInstance(talksConfig.getDao());
    private final StoresUtils storesUtils = getInstance();
    private final FileSystemApi filesystem = FileSystem.getInstance(talksConfig);
    private final Plotter plotter = Plotter.getInstance();

    public FunTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext();
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.of(
            InputHandler.of("fun.FunTalk::menu",
                InputTrigger.ofRegex("/plot"))
        );
    }

    @Override
    public TalkResponse menu(TalkRequest request) {
        return functions(request);
    }

    // MAIN FUN MENU

    public TalkResponse functions(TalkRequest request) {
        context.set(request.getChatId(), DISPLAY_FUN_MODE, DISPLAY_FUN_MODE_EDIT);
        context.set(request.getChatId(), BOX_ATTRIBUTES_EDITING, BOX_ATTRIBUTES_EDITING_FUN_MENU);

        return TalkResponse.ofText(
                request.getChatId(),
                "Plot the Fun:"
            ).withButton("fun.FunTalk::funYisX", "Y = X")
             .withButton("fun.FunTalk::funCircle", "Circle")
             .withButton("fun.FunTalk::funEllipse", "Ellipse")
             .withButton("fun.FunTalk::funFlower", "Flower")
             .withButton("fun.FunTalk::funSquareRoot", "Square Root")
             .withButton("fun.FunTalk::freeFun", "🛶 Free")
             .withButton("fun.FunTalk::range", "⚙️ min..max")
             .withButton("fun.FunTalk::style", "⚙️ Style")
             .withButton("intro.WelcomeTalk::menu", "🏕 Back");
    }

    // X/P RANGE

    public TalkResponse range(TalkRequest request) {
        return TalkResponse.ofText(
                request.getChatId(),
                "Set X/P range:"
            ).withButton("fun.FunTalk::minLimit", "⚙ Min X/P")
             .withButton("fun.FunTalk::maxLimit", "⚙ Max X/P")
             .withButton("fun.FunTalk::" + getBoxAttributesBackMethod(request), "🔙 Back");
    }

    public TalkResponse minLimit(TalkRequest request) {
        Double minLimit = getMinLimit(request, null, null);

        String basePrompt = "Enter new min X or P, or tap on /range\n" +
            "\n" +
            usefulValues();
        String prompt = basePrompt + (minLimit == null ? "" :
            "\n" +
            "\n" +
            "Current min is " + minLimit);

        return TalkResponse.ofText(
            request.getChatId(),
            prompt
        ).withRegex("fun.FunTalk::setMinLimit", Triggers.FLOATING_POINT)
         .withString("fun.FunTalk::range", "/range");
    }

    public TalkResponse setMinLimit(TalkRequest request) {
        double limit;
        try {
            limit = Double.parseDouble(request.getMessage().text().trim());
        } catch (NumberFormatException e) {
            return wrongLimit(request);
        }

        context.set(request.getChatId(), IV_MIN, limit);

        return TalkResponse.ofText(
            request.getChatId(),
            "Min X or P set to " + limit
        ).withButton("fun.FunTalk::range", "Continue");
    }

    public TalkResponse maxLimit(TalkRequest request) {
        Double maxLimit = getMaxLimit(request, null, null);

        String basePrompt = "Enter max X or P, or tap on /range\n" +
            "\n" +
            usefulValues();
        String prompt = basePrompt + (maxLimit == null ? "" :
            "\n" +
            "\n" +
            "Current max is " + maxLimit);

        return TalkResponse.ofText(
            request.getChatId(),
            prompt
        ).withRegex("fun.FunTalk::setMaxLimit", Triggers.FLOATING_POINT)
         .withString("fun.FunTalk::range", "/range");
    }

    public TalkResponse setMaxLimit(TalkRequest request) {
        double limit;
        try {
            limit = Double.parseDouble(request.getMessage().text().trim());
        } catch (NumberFormatException e) {
            return wrongLimit(request);
        }

        context.set(request.getChatId(), IV_MAX, limit);

        return TalkResponse.ofText(
            request.getChatId(),
            "Max X or P set to " + limit
        ).withButton("fun.FunTalk::range", "Continue");
    }

    private String usefulValues() {
        String usefulValues = "Useful values:\n" +
            "  PI = 3.1416\n" +
            "  2 * PI = 6.2832";
        return usefulValues;
    }

    private TalkResponse wrongLimit(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "Can not parse limit, please retry or tap on /range"
        ).withPreserveHandlers();
    }

    // STYLE

    public TalkResponse style(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "Set style parameters:"
        ).withButton("fun.FunTalk::lineColor", "Line color")
         .withButton("fun.FunTalk::thickness", "Line thickness")
         .withButton("fun.FunTalk::lineStyle", "Line style")
         .withButton("fun.FunTalk::background", "Background")
         .withButton("fun.FunTalk::axes", "Axes")
         .withButton("fun.FunTalk::" + getBoxAttributesBackMethod(request), "🔙 Back");
    }

    public TalkResponse lineColor(TalkRequest request) {
        TalkResponse response = TalkResponse.ofText(
            request.getChatId(),
            "Choose line color or enter #rrggbb:"
        ).withRegex("fun.FunTalk::doLineColor", Triggers.RGB_COLOR);

        return decorateWithColors(response, "doLineColor");
    }

    public TalkResponse doLineColor(TalkRequest request) {
        String text = request.getMessage().text();
        String rgbCode = text.startsWith("#") ? text : PlotterColor.valueOf(text).getRgbCode();

        context.set(request.getChatId(), LINE_COLOR, rgbCode);

        return TalkResponse.ofText(
            request.getChatId(),
            "Line color set to " + text + "."
        ).withButton("fun.FunTalk::style", "Continue");
    }

    public TalkResponse thickness(TalkRequest request) {
        TalkResponse response = TalkResponse.ofText(
            request.getChatId(),
            "Choose line thickness in pixels:"
        );

        for (Integer thickness : Thickness.values) {
            response.withButton("fun.FunTalk::doThickness", thickness.toString());
        }

        return response;
    }

    public TalkResponse doThickness(TalkRequest request) {
        String text = request.getMessage().text();
        int thickness = Integer.parseInt(text);

        context.set(request.getChatId(), LINE_THICKNESS, thickness);

        return TalkResponse.ofText(
            request.getChatId(),
            "Line thickness set to " + thickness + " pixels."
        ).withButton("fun.FunTalk::style", "Continue");
    }

    public TalkResponse lineStyle(TalkRequest request) {
        TalkResponse response = TalkResponse.ofText(
            request.getChatId(),
            "Choose line style:"
        );

        for (LineStyle lineStyle : LineStyle.values()) {
            response.withButton("fun.FunTalk::doLineStyle", lineStyle.getName());
        }

        return response;
    }

    public TalkResponse doLineStyle(TalkRequest request) {
        String text = request.getMessage().text();
        LineStyle lineStyle = LineStyle.fromName(text);

        context.set(request.getChatId(), LINE_STYLE, lineStyle.name());

        return TalkResponse.ofText(
            request.getChatId(),
            "Line style set to " + lineStyle.getName() + "."
        ).withButton("fun.FunTalk::style", "Continue");
    }

    public TalkResponse background(TalkRequest request) {
        TalkResponse response = TalkResponse.ofText(
            request.getChatId(),
            "Choose background color or enter #rrggbb:"
        ).withRegex("fun.FunTalk::doBackground", Triggers.RGB_COLOR);

        return decorateWithColors(response, "doBackground");
    }

    public TalkResponse doBackground(TalkRequest request) {
        String text = request.getMessage().text();
        String rgbCode = text.startsWith("#") ? text : PlotterColor.valueOf(text).getRgbCode();

        context.set(request.getChatId(), BACKGROUND_COLOR, rgbCode);

        return TalkResponse.ofText(
            request.getChatId(),
            "Background color set to " + text + "."
        ).withButton("fun.FunTalk::style", "Continue");
    }

    public TalkResponse axes(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "Toggle axes:"
        ).withButton("fun.FunTalk::doAxes", "On")
         .withButton("fun.FunTalk::doAxes", "Off");
    }

    public TalkResponse doAxes(TalkRequest request) {
        String text = request.getMessage().text();
        int displayAxes = "On".equals(text) ? 1 : 0;

        context.set(request.getChatId(), DISPLAY_AXES, displayAxes);

        return TalkResponse.ofText(
            request.getChatId(),
            "Display axes set to " + text + "."
        ).withButton("fun.FunTalk::style", "Continue");
    }

    private TalkResponse decorateWithColors(TalkResponse response, String methodName) {
        for (PlotterColor color : PlotterColor.values()) {
            response.withButton("fun.FunTalk::" + methodName, color.name());
        }
        return response;
    }

    // FUNS

    public TalkResponse funYisX(TalkRequest request) {
        showWait(request, "Y = X");

        resetBoxInfo(request);

        String funGenerator = "YisX";
        double defaultMinX = -1.0;
        double defaultMaxX = 1.0;
        double minLimit = getMinLimit(request, funGenerator, defaultMinX);
        double maxLimit = getMaxLimit(request, funGenerator, defaultMaxX);

        FreeFun fun = defunYisX();
        Graph graph = new Graph(GRAPH_SIZE, GRAPH_SIZE,
            minLimit, maxLimit,
            PlotStyle.fromFunContext(context, request.getChatId()),
            fun);

        saveFreeFunInContext(request, fun);

        try {
            plotter.plot(graph, fun);
            filesystem.deleteAfter(graph.file, 14, TimeUnit.SECONDS);
        } catch (NotFunException e) {
            return cantCreateImage(request, e);
        }

        request.getInputAdapter().sendResponse(
            TalkResponse.ofPhoto(
                request.getChatId(),
                graph.file
            ).withPreserveHandlers()
        );

        return funShown(request, fun, funGenerator, minLimit, maxLimit, graph);
    }

    private FreeFun defunYisX() {
        JelProxy jel = new JelProxy(IndependentVariable.X);

        ConcreteFreeFunXY funYisX = new ConcreteFreeFunXY(jel);

        funYisX.setYandCompile("x");

        return funYisX;
    }

    private Fun defunPlainYisX() {
        return new FunXY() {
            @Override
            public Function<Double, Double> y() {
                return Fun.Y_IS_P;
            }
        };
    }

    public TalkResponse funFlower(TalkRequest request) {
        showWait(request, "Flower");

        resetBoxInfo(request);

        String funGenerator = "Flower";
        double defaultMinP = 0.0;
        double defaultMaxP = 2.0 * Math.PI;
        double minLimit = getMinLimit(request, funGenerator, defaultMinP);
        double maxLimit = getMaxLimit(request, funGenerator, defaultMaxP);

        FreeFun fun = defunFlower();
        Graph graph = new Graph(GRAPH_SIZE, GRAPH_SIZE,
            minLimit, maxLimit,
            PlotStyle.fromFunContext(context, request.getChatId()),
            fun);

        saveFreeFunInContext(request, fun);

        try {
            plotter.plot(graph, fun);
            filesystem.deleteAfter(graph.file, 14, TimeUnit.SECONDS);
        } catch (NotFunException e) {
            return cantCreateImage(request, e);
        }

        request.getInputAdapter().sendResponse(
            TalkResponse.ofPhoto(
                request.getChatId(),
                graph.file
            ).withPreserveHandlers()
        );

        return funShown(request, fun, funGenerator, minLimit, maxLimit, graph);
    }

    private FreeFun defunFlower() {
        JelProxy jel = new JelProxy(IndependentVariable.P);

        ConcreteFreeFunXYP funEllipse = new ConcreteFreeFunXYP(jel);

        funEllipse.setXandCompile("cos(10*p) + sin(p)");
        funEllipse.setYandCompile("sin(10*p) + cos(p)");

        return funEllipse;
    }

    public TalkResponse funSquareRoot(TalkRequest request) {
        showWait(request, "Square Root");

        resetBoxInfo(request);

        String funGenerator = "SquareRoot";
        double defaultMinX = -20.0;
        double defaultMaxX = 20.0;
        double minLimit = getMinLimit(request, funGenerator, defaultMinX);
        double maxLimit = getMaxLimit(request, funGenerator, defaultMaxX);

        FreeFun fun = defunSquareRoot();
        Graph graph = new Graph(GRAPH_SIZE, GRAPH_SIZE,
            minLimit, maxLimit,
            PlotStyle.fromFunContext(context, request.getChatId()),
            fun);

        saveFreeFunInContext(request, fun);

        try {
            plotter.plot(graph, fun);
            filesystem.deleteAfter(graph.file, 14, TimeUnit.SECONDS);
        } catch (NotFunException e) {
            return cantCreateImage(request, e);
        }

        request.getInputAdapter().sendResponse(
            TalkResponse.ofPhoto(
                request.getChatId(),
                graph.file
            ).withPreserveHandlers()
        );

        return funShown(request, fun, funGenerator, minLimit, maxLimit, graph);
    }

    private FreeFun defunSquareRoot() {
        JelProxy jel = new JelProxy(IndependentVariable.X);

        ConcreteFreeFunXY funSquareRoot = new ConcreteFreeFunXY(jel);

        funSquareRoot.setYandCompile("sqrt(x)");

        return funSquareRoot;
    }

    public TalkResponse funCircle(TalkRequest request) {
        showWait(request, "Circle");

        resetBoxInfo(request);

        String funGenerator = "Circle";
        double defaultMinP = 0.0;
        double defaultMaxP = 2.0 * Math.PI;
        double minLimit = getMinLimit(request, funGenerator, defaultMinP);
        double maxLimit = getMaxLimit(request, funGenerator, defaultMaxP);

        FreeFun fun = defunCircle();
        Graph graph = new Graph(GRAPH_SIZE, GRAPH_SIZE,
            minLimit, maxLimit,
            PlotStyle.fromFunContext(context, request.getChatId()),
            fun);

        saveFreeFunInContext(request, fun);

        try {
            plotter.plot(graph, fun);
            filesystem.deleteAfter(graph.file, 14, TimeUnit.SECONDS);
        } catch (NotFunException e) {
            return cantCreateImage(request, e);
        }

        request.getInputAdapter().sendResponse(
            TalkResponse.ofPhoto(
                request.getChatId(),
                graph.file
            ).withPreserveHandlers()
        );

        return funShown(request, fun, funGenerator, minLimit, maxLimit, graph);
    }

    private FreeFun defunCircle() {
        JelProxy jel = new JelProxy(IndependentVariable.P);

        ConcreteFreeFunXYP funCircle = new ConcreteFreeFunXYP(jel);

        funCircle.setXandCompile("cos(p)");
        funCircle.setYandCompile("sin(p)");

        return funCircle;
    }

    private Fun defunPlainCircle() {
        return new FunXYP() {
            @Override
            public Function<Double, Double> x() {
                return Math::cos;
            }
            @Override
            public Function<Double, Double> y() {
                return Math::sin;
            }
        };
    }

    public TalkResponse funEllipse(TalkRequest request) {
        showWait(request, "Ellipse");

        resetBoxInfo(request);

        String funGenerator = "Ellipse";
        double defaultMinP = 0.0;
        double defaultMaxP = 2.0 * Math.PI;
        double minLimit = getMinLimit(request, funGenerator, defaultMinP);
        double maxLimit = getMaxLimit(request, funGenerator, defaultMaxP);

        FreeFun fun = defunEllipse();
        Graph graph = new Graph(GRAPH_SIZE, GRAPH_SIZE,
            minLimit, maxLimit,
            PlotStyle.fromFunContext(context, request.getChatId()),
            fun);

        saveFreeFunInContext(request, fun);

        try {
            plotter.plot(graph, fun);
            filesystem.deleteAfter(graph.file, 14, TimeUnit.SECONDS);
        } catch (NotFunException e) {
            return cantCreateImage(request, e);
        }

        request.getInputAdapter().sendResponse(
            TalkResponse.ofPhoto(
                request.getChatId(),
                graph.file
            ).withPreserveHandlers()
        );

        return funShown(request, fun, funGenerator, minLimit, maxLimit, graph);
    }

    private FreeFun defunEllipse() {
        JelProxy jel = new JelProxy(IndependentVariable.P);

        ConcreteFreeFunXYP funEllipse = new ConcreteFreeFunXYP(jel);

        funEllipse.setXandCompile("cos(p)");
        funEllipse.setYandCompile("sin(p) / 2");

        return funEllipse;
    }

    private Fun defunPlainEllipse() {
        return new FunXYP() {
            @Override
            public Function<Double, Double> x() {
                return Math::cos;
            }
            @Override
            public Function<Double, Double> y() {
                return p -> Math.sin(p) / 2.0;
            }
        };
    }

    // FREE FUNS

    public TalkResponse freeFun(TalkRequest request) {
        resetBoxInfo(request);

        return TalkResponse.ofText(
            request.getChatId(),
            "Select fun type"
        ).withButton("fun.FunTalk::freeFunXY", "XY")
         .withButton("fun.FunTalk::freeFunXYP", "XYP");
    }

    public TalkResponse freeFunXY(TalkRequest request) {
        context.set(request.getChatId(), FUN_TYPE, "XY");
        context.set(request.getChatId(), FUN_DV_IV, "Y_X");

        String expressionY = context.getString(request.getChatId(), FUN_Y_BY_X);

        return TalkResponse.ofText(
            request.getChatId(),
            "Enter y = f(x)\n" +
                "\n" +
                "You can see /hints" +
                (expressionY == null ? "" : " or /reuse y = " + expressionY)
        ).withRegex("fun.FunTalk::gotFreeFunYbyX", Triggers.NOT_A_COMMAND)
         .withString("fun.FunTalk::expressionHints", "/hints")
         .withString("fun.FunTalk::gotReuseFreeFunYbyX", "/reuse");
    }

    public TalkResponse gotFreeFunYbyX(TalkRequest request) {
        context.set(request.getChatId(), "FUN_Y_BY_X", cleanRightSide(request.getMessage().text(), "y", "x"));

        return gotReuseFreeFunYbyX(request);
    }

    public TalkResponse gotReuseFreeFunYbyX(TalkRequest request) {
        return doFreeFun(request);
    }

    public TalkResponse freeFunXYP(TalkRequest request) {
        context.set(request.getChatId(), FUN_TYPE, "XYP");
        context.set(request.getChatId(), FUN_DV_IV, "X_P");

        String expressionX = context.getString(request.getChatId(), FUN_X_BY_P);

        return TalkResponse.ofText(
            request.getChatId(),
            "Enter x = f(p)\n" +
                "\n" +
                "You can see /hints" +
                (expressionX == null ? "" : " or /reuse x = " + expressionX)
        ).withRegex("fun.FunTalk::gotFreeFunXbyP", Triggers.NOT_A_COMMAND)
         .withString("fun.FunTalk::expressionHints", "/hints")
         .withString("fun.FunTalk::gotReuseFreeFunXbyP", "/reuse");
    }

    public TalkResponse gotFreeFunXbyP(TalkRequest request) {
        context.set(request.getChatId(), FUN_X_BY_P, cleanRightSide(request.getMessage().text(), "x", "p"));

        return gotReuseFreeFunXbyP(request);
    }

    public TalkResponse gotReuseFreeFunXbyP(TalkRequest request) {
        context.set(request.getChatId(), FUN_DV_IV, "Y_P");

        String expressionY = context.getString(request.getChatId(), FUN_Y_BY_P);

        return TalkResponse.ofText(
            request.getChatId(),
            "Enter y = f(p)\n" +
                "\n" +
                "You can see /hints" +
                (expressionY == null ? "" : " or /reuse y = " + expressionY)
        ).withRegex("fun.FunTalk::gotFreeFunYbyP", Triggers.NOT_A_COMMAND)
         .withString("fun.FunTalk::expressionHints", "/hints")
         .withString("fun.FunTalk::gotReuseFreeFunYbyP", "/reuse");
    }

    public TalkResponse gotFreeFunYbyP(TalkRequest request) {
        context.set(request.getChatId(), FUN_Y_BY_P, cleanRightSide(request.getMessage().text(), "y", "p"));

        return gotReuseFreeFunYbyP(request);
    }

    public TalkResponse gotReuseFreeFunYbyP(TalkRequest request) {
        return doFreeFun(request);
    }

    public TalkResponse expressionHints(TalkRequest request) {
        request.getInputAdapter().sendResponse(
            TalkResponse.ofHtml(
                request.getChatId(),
                "Available functions reference <a href=\"https://docs.oracle.com/javase/8/docs/api/java/lang/Math.html\">here</a>\n" +
                    "\n" +
                    "You can use every static method and constant\n" +
                    "\n" +
                    "Independent variable is <code>x</code> for <b>XY</b> fun type and <code>p</code> for <b>XYP</b> fun type\n" +
                    "\n" +
                    "Some examples:\n" +
                    "  Cuadratic: <code>y = pow(x, 2)</code>\n" +
                    "  Square root: <code>y = sqrt(x)</code>\n" +
                    "  Circle: <code>x = cos(p)</code>, <code>y = sin(p)</code>"
            ).withPreserveHandlers()
        );

        String calleeContext = context.getString(request.getChatId(), FUN_DV_IV);

        switch (calleeContext) {
            case "Y_X":
                return freeFunXY(request);
            case "X_P":
                return freeFunXYP(request);
            case "Y_P":
                return gotReuseFreeFunXbyP(request);
            default:
                throw new NotFunException("callee context not available");
        }
    }

    private String cleanRightSide(String expression, String dependentVariable, String independentVariable) {
        String fOfIndependentVariableRegex = "f *\\( *" + independentVariable + " *\\)";
        String leftSideRegex = " *(" + dependentVariable + "|" + fOfIndependentVariableRegex + ") *=";

        return expression
            .replaceFirst(leftSideRegex, "")
            .replaceAll(" ", "")
            .replaceAll("([+*/-])", " $1 ");
    }

    public TalkResponse doFreeFun(TalkRequest request) {
        showWait(request, "freefun");

        String funType = context.getString(request.getChatId(), FUN_TYPE);

        if ("XY".equals(funType)) {
            return doFreeFunXY(request);
        } else {
            return doFreeFunXYP(request);
        }
    }

    public TalkResponse doFreeFunXY(TalkRequest request) {
        String funYbyX = context.getString(request.getChatId(), FUN_Y_BY_X);

        String funGenerator = "FreeFunXY";
        double defaultMinX = -1.0;
        double defaultMaxX = 1.0;
        double minLimit = getMinLimit(request, funGenerator, defaultMinX);
        double maxLimit = getMaxLimit(request, funGenerator, defaultMaxX);

        FreeFun fun;
        try {
            fun = defunFreeFunXY(funYbyX);
        } catch (FunException e) {
            return compilationError(request, e);
        }

        Graph graph = new Graph(GRAPH_SIZE, GRAPH_SIZE,
            minLimit, maxLimit,
            PlotStyle.fromFunContext(context, request.getChatId()),
            fun);

        saveFreeFunInContext(request, fun);

        try {
            plotter.plot(graph, fun);
            filesystem.deleteAfter(graph.file, 14, TimeUnit.SECONDS);
        } catch (NotFunException e) {
            return cantCreateImage(request, e);
        }

        request.getInputAdapter().sendResponse(
            TalkResponse.ofPhoto(
                request.getChatId(),
                graph.file
            ).withPreserveHandlers()
        );

        return funShown(request, fun, funGenerator, minLimit, maxLimit, graph);
    }

    private FreeFun defunFreeFunXY(String fun) {
        JelProxy jel = new JelProxy(IndependentVariable.X);

        ConcreteFreeFunXY freeFun = new ConcreteFreeFunXY(jel);

        freeFun.setYandCompile(fun);

        return freeFun;
    }

    public TalkResponse doFreeFunXYP(TalkRequest request) {
        String funXbyP = context.getString(request.getChatId(), FUN_X_BY_P);
        String funYbyP = context.getString(request.getChatId(), FUN_Y_BY_P);

        String funGenerator = "FreeFunXYP";
        double defaultMinP = 0.0;
        double defaultMaxP = 2.0 * Math.PI;
        double minLimit = getMinLimit(request, funGenerator, defaultMinP);
        double maxLimit = getMaxLimit(request, funGenerator, defaultMaxP);

        FreeFun fun;
        try {
            fun = defunFreeFunXYP(funXbyP, funYbyP);
        } catch (FunException e) {
            return compilationError(request, e);
        }

        Graph graph = new Graph(GRAPH_SIZE, GRAPH_SIZE,
            minLimit, maxLimit,
            PlotStyle.fromFunContext(context, request.getChatId()),
            fun);

        try {
            plotter.plot(graph, fun);
            filesystem.deleteAfter(graph.file, 14, TimeUnit.SECONDS);
        } catch (NotFunException e) {
            return cantCreateImage(request, e);
        }

        saveFreeFunInContext(request, fun);

        request.getInputAdapter().sendResponse(
            TalkResponse.ofPhoto(
                request.getChatId(),
                graph.file
            ).withPreserveHandlers()
        );

        return funShown(request, fun, funGenerator, minLimit, maxLimit, graph);
    }

    private FreeFun defunFreeFunXYP(String xFun, String yFun) {
        JelProxy jel = new JelProxy(IndependentVariable.P);

        ConcreteFreeFunXYP fun = new ConcreteFreeFunXYP(jel);

        fun.setXandCompile(xFun);
        fun.setYandCompile(yFun);

        return fun;
    }

    // STORED FUNS

    public TalkResponse freeStoredFun(
        TalkRequest request,
        String displayMode,
        Long boxId,
        String name, String tagNames,
        boolean showcased,
        String type, String expressionX, String expressionY,
        String rangeStart, String rangeEnd,
        PlotStyle plotStyle
    ) {
        context.set(request.getChatId(), BOX_ID, boxId);
        context.set(request.getChatId(), FUN_NAME, name);
        context.set(request.getChatId(), FUN_TAGS, tagNames);
        context.set(request.getChatId(), FUN_TYPE, type);
        context.set(request.getChatId(), IV_MIN, rangeStart);
        context.set(request.getChatId(), IV_MAX, rangeEnd);
        PlotStyle.saveInFunContext(context, request.getChatId(), plotStyle);

        if ("XY".equals(type)) {
            context.set(request.getChatId(), FUN_Y_BY_X, expressionY);
        } else {
            context.set(request.getChatId(), FUN_X_BY_P, expressionX);
            context.set(request.getChatId(), FUN_Y_BY_P, expressionY);
        }

        context.set(request.getChatId(), DISPLAY_FUN_MODE, displayMode);

        return doFreeFun(request);
    }

    // AFTER FUN DISPLAYED

    private TalkResponse funShown(TalkRequest request, FreeFun fun, String funGenerator, Double minLimit, Double maxLimit, Graph graph) {
        Long boxId = context.getLong(request.getChatId(), BOX_ID);
        String name = context.getString(request.getChatId(), FUN_NAME);
        String tagNames = context.getString(request.getChatId(), FUN_TAGS);
        String displayMode = context.getString(request.getChatId(), DISPLAY_FUN_MODE);
        String funDescription = boxId != null && DISPLAY_FUN_MODE_LIKE.equals(displayMode) ?
            storesUtils.fullBoxDescription(storeService.findBox(boxId).get()) :
            fun.toString();

        saveLimitsInContext(request, funGenerator, minLimit, maxLimit);
        context.set(request.getChatId(), FUN_DESCRIPTION, funDescription);

        storesUtils.saveInContext(context, request,
            boxId,
            name, tagNames,
            fun.type(),
            fun.getXexpression(), fun.getYexpression(),
            minLimit.toString(), maxLimit.toString(),
            PlotStyle.fromFunContext(context, request.getChatId())
        );

        return funShownMenu(request);
    }

    public TalkResponse funShownMenu(TalkRequest request) {
        String displayMode = context.getString(request.getChatId(), DISPLAY_FUN_MODE);
        String funDescription = context.getString(request.getChatId(), FUN_DESCRIPTION);

        TalkResponse response = TalkResponse.ofText(
            request.getChatId(),
            funDescription
        );

        if (DISPLAY_FUN_MODE_LIKE.equals(displayMode)) {
            response
                .withButton("fun.ShowcaseTalk::like", "Like ⭐")
                .withButton("fun.ShowcaseTalk::showNext", "Next")
                .withButton("fun.MyStoreTalk::store", "Copy")
                .withButton("fun.ShowcaseTalk::exploreShowcasedFuns", "Explore");

        } else {
            context.set(request.getChatId(), BOX_ATTRIBUTES_EDITING, BOX_ATTRIBUTES_EDITING_AFTER_SHOWN);

            response
                .withButton("fun.FunTalk::range", "⚙️ min..max")
                .withButton("fun.FunTalk::style", "⚙️ Style")
                .withButton("fun.FunTalk::edit", "🔣 Edit")
                .withButton("fun.MyStoreTalk::store", "☘ Save")
                .withButton("fun.FunTalk::menu", "✏ Back to funs")
                .withButton("fun.MyStoreTalk::myStore", "☘ My Store");
        }

        return response;
    }

    public TalkResponse edit(TalkRequest request) {
        String funType = context.getString(request.getChatId(), FUN_TYPE);

        if ("XY".equals(funType)) {
            return freeFunXY(request);
        } else {
            return freeFunXYP(request);
        }
    }

    // UTILS

    private String getBoxAttributesBackMethod(TalkRequest request) {
        String boxAttributesEditing = context.getString(request.getChatId(), BOX_ATTRIBUTES_EDITING);
        return BOX_ATTRIBUTES_EDITING_AFTER_SHOWN.equals(boxAttributesEditing) ?
            "doFreeFun" :
            "functions";
    }

    private void resetBoxInfo(TalkRequest request) {
        context.remove(request.getChatId(), BOX_ID);
        context.remove(request.getChatId(), FUN_NAME);
        context.remove(request.getChatId(), FUN_TAGS);
    }

    private Double getMinLimit(TalkRequest request, String funGenerator, Double defaultValue) {
        String lastGenerator = context.getString(request.getChatId(), FUN_GENERATOR);
        boolean getLimitFromContext = funGenerator == null || lastGenerator == null ||
            funGenerator.equals(lastGenerator) || funGenerator.startsWith("FreeFun");
        Double minLimit = getLimitFromContext ? context.getDouble(request.getChatId(), IV_MIN) : defaultValue;
        return minLimit == null ? defaultValue : minLimit;
    }

    private Double getMaxLimit(TalkRequest request, String funGenerator, Double defaultValue) {
        String lastGenerator = context.getString(request.getChatId(), FUN_GENERATOR);
        boolean getLimitFromContext = funGenerator == null || lastGenerator == null ||
            funGenerator.equals(lastGenerator) || funGenerator.startsWith("FreeFun");
        Double maxLimit = getLimitFromContext ? context.getDouble(request.getChatId(), IV_MAX) : defaultValue;
        return maxLimit == null ? defaultValue : maxLimit;
    }

    private void saveFreeFunInContext(TalkRequest request, FreeFun fun) {
        if (fun instanceof ConcreteFreeFunXY) {
            context.set(request.getChatId(), FUN_TYPE, "XY");
            context.set(request.getChatId(), FUN_Y_BY_X, fun.getYexpression());
        } else if (fun instanceof ConcreteFreeFunXYP) {
            context.set(request.getChatId(), FUN_TYPE, "XYP");
            context.set(request.getChatId(), FUN_X_BY_P, fun.getXexpression());
            context.set(request.getChatId(), FUN_Y_BY_P, fun.getYexpression());
        }
    }

    private void saveLimitsInContext(TalkRequest request, String funGenerator, Double minLimit, Double maxLimit) {
        context.set(request.getChatId(), FUN_GENERATOR, funGenerator);
        context.set(request.getChatId(), IV_MIN, minLimit);
        context.set(request.getChatId(), IV_MAX, maxLimit);
    }

    // GENERIC RESPONSES

    private void showWait(TalkRequest request, String caption) {
        request.getInputAdapter().sendResponse(
            TalkResponse.ofText(
                request.getChatId(),
                "⏳⏳ " + caption + " ⏳⏳"
            )
        );
    }

    private TalkResponse compilationError(TalkRequest request, FunException e) {
        return TalkResponse.ofText(
            request.getChatId(),
            e.getMessage()
        ).withButton("fun.FunTalk::freeFun", "Retry")
         .withButton("fun.FunTalk::menu", "🏕 Back");
    }

    private TalkResponse cantCreateImage(TalkRequest request, NotFunException e) {
        return TalkResponse.ofText(
            request.getChatId(),
            "System error. Unable to paint image."
        ).withButton("fun.FunTalk::menu", "🏕 Back");
    }
}
