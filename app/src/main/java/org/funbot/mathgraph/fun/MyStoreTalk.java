package org.funbot.mathgraph.fun;

import org.funbot.mathgraph.Triggers;
import org.funbot.mathgraph.admin.AuthService;
import org.funbot.mathgraph.admin.User;
import org.funbot.mathgraph.plot.PlotStyle;
import org.funbot.mathgraph.stores.*;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.InputTrigger;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static org.funbot.mathgraph.stores.StoresUtils.*;

public class MyStoreTalk extends Talk {

    private AuthService authService;
    private StoreService storeService;
    private StoresUtils storesUtils;

    public MyStoreTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("FunTalk");

        authService = AuthService.getInstance(talksConfig);
        storeService = StoreService.getInstance(talksConfig.getDao());
        storesUtils = StoresUtils.getInstance();
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.of(
            InputHandler.of("fun.MyStoreTalk::myStore",
                InputTrigger.ofRegex("/store"))
        );
    }

    // MY STORE FRONT

    public TalkResponse myStore(TalkRequest request) {
        User user = authService.findUser(request.getChatId());
        List<StoredBox> boxes = storeService.getUserBoxes(user);

        String prompt = boxes.isEmpty() ?
            "You don´t have funs in Store" :
            "Your funs: " +
                "\n" +
                "\n" + boxes.stream().map(this::oneLineBoxDescription).collect(Collectors.joining("\n"));

        return TalkResponse.ofText(
            request.getChatId(),
            prompt
        ).withRegex("fun.MyStoreTalk::showBox", Triggers.BOX_ID_COMMAND)
         .withButton("fun.FunTalk::menu", "✏ Back to funs")
         .withButton("fun.ShowcaseTalk::exploreShowcasedFuns", "🚴 Showcase");
    }

    private String oneLineBoxDescription(StoredBox storedBox) {
        return storedBox.getName() + " /box_" + storesUtils.hashId(storedBox.getId()) +
            (storedBox.getShowcasedAt() == null ? "" : " [showcased]");
    }

    // SHOW BOX

    public TalkResponse showBox(TalkRequest request) {
        String command = request.getMessage().text();
        String hashId = storesUtils.extractTrailingHashId(command);
        Long boxId = storesUtils.unhashId(hashId);

        Optional<StoredBox> boxOpt = storeService.findBox(boxId);

        if (! boxOpt.isPresent()) {
            return boxNotFound(request);
        }

        StoredBox box = boxOpt.get();

        context.set(request.getChatId(), STORE_BOX_ID, box.getId());
        context.set(request.getChatId(), STORE_NAME, box.getName());

        return TalkResponse.ofText(
            request.getChatId(),
            storesUtils.fullBoxDescription(box)
        ).withButton("fun.MyStoreTalk::plotIt", "Edit this Plot it!")
         .withButtonIf("fun.ShowcaseTalk::showcaseFun", "Showcase!", !box.showcased())
         .withButtonIf("fun.MyStoreTalk::drop", "Drop", !box.showcased())
         .withButton("fun.MyStoreTalk::myStore", "Back to My Store");
    }

    private TalkResponse boxNotFound(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "Fun not found."
        ).withPreserveHandlers();
    }

    // MOVE TO PLOT

    public TalkResponse plotIt(TalkRequest request) {
        Long boxId = context.getLong(request.getChatId(), STORE_BOX_ID);
        StoredBox box = storeService.findBox(boxId).get();
        StoredFun fun = box.getFun();

        if (box.showcased()) {
            Long showcaseId = storeService.getShowcaseId(boxId);
            context.set(request.getChatId(), StoresUtils.STORE_SHOWCASE_ID, showcaseId);
        }

        return talkForClass(FunTalk.class).freeStoredFun(
            request,
            FunTalk.DISPLAY_FUN_MODE_EDIT,
            box.getId(),
            box.getName(),
            box.getTagsDescription(),
            box.showcased(),
            fun.getType(),
            fun.getExpressionX(),
            fun.getExpressionY(),
            box.getRangeStart(),
            box.getRangeEnd(),
            box.getPlotStyle()
        );
    }

    // DROP

    public TalkResponse drop(TalkRequest request) {
        String name = context.getString(request.getChatId(), STORE_NAME);

        return TalkResponse.ofText(
            request.getChatId(),
            "Are you sure to drop " + name + "?"
        ).withButton("fun.MyStoreTalk::dropIt", "Yes")
         .withButton("fun.MyStoreTalk::myStore", "No");
    }

    public TalkResponse dropIt(TalkRequest request) {
        Long boxId = context.getLong(request.getChatId(), STORE_BOX_ID);

        try {
            storeService.delete(boxId);

            return TalkResponse.ofText(
                request.getChatId(),
                "Fun dropped."
            ).withButton("fun.MyStoreTalk::myStore", "Continue");

        } catch (Exception e) {
            return databaseError(request, e);
        }
    }

    // STORE FUNS

    public TalkResponse store(TalkRequest request) {
        context.set(request.getChatId(), FunTalk.DISPLAY_FUN_MODE, FunTalk.DISPLAY_FUN_MODE_EDIT);

        Long boxId = context.getLong(request.getChatId(), STORE_BOX_ID);

        if (boxId == null) {
            return storeNewFun(request);
        }

        StoredBox box = storeService.findBox(boxId).get();

        if (box.showcased()) {
            return storeFunCopy(request, boxId);
        }

        return overwriteFun(request, boxId);
    }

    private TalkResponse storeNewFun(TalkRequest request) {
        return storeAskName(request);
    }

    private TalkResponse overwriteFun(TalkRequest request, Long boxId) {
        return storeAskName(request);
    }

    private TalkResponse storeFunCopy(TalkRequest request, Long boxId) {
        return TalkResponse.ofText(
            request.getChatId(),
                "Fun is showcased and can't be overwritten. Save it as a new fun?"
            ).withButton("fun.FunTalk::menu", "Nooo")
             .withButton("fun.MyStoreTalk::storeAsNewFun", "Yes!");
    }

    public TalkResponse storeAsNewFun(TalkRequest request) {
        context.remove(request.getChatId(), STORE_BOX_ID);

        return storeAskName(request);
    }

    public TalkResponse storeAskName(TalkRequest request) {
        String name = context.getString(request.getChatId(), STORE_NAME);

        return TalkResponse.ofText(
            request.getChatId(),
            "Fun name? (alphanumeric, can contain spaces)" +
                (name == null ? "" : "\n\nOr /reuse current name: " + name)
        ).withRegex("fun.MyStoreTalk::storeAskTagsSavingName", Triggers.NOT_A_COMMAND)
         .withStringIf("fun.MyStoreTalk::storeAskTags", "/reuse", name != null);
    }

    public TalkResponse storeAskTagsSavingName(TalkRequest request) {
        String funName = normalizeName(request.getMessage().text());

        if (!nameIsValid(funName)) {
            return TalkResponse.ofText(
                request.getChatId(),
                "Invalid name format. Try again."
            ).withButton("fun.MyStoreTalk::storeAskName", "Continue");
        }

        context.set(request.getChatId(), STORE_NAME, funName);

        return storeAskTags(request);
    }

    private String normalizeName(String name) {
        return name.trim()
            .replaceAll("  +", " ");
    }

    private boolean nameIsValid(String tag) {
        return tag.matches("[a-zA-Z0-9 ]+");
    }

    public TalkResponse storeAskTags(TalkRequest request) {
        String tags = context.getString(request.getChatId(), STORE_TAGS);

        return TalkResponse.ofText(
            request.getChatId(),
            "Fun tags? (comma-separated, alphanumeric, can contain spaces, or /none)" +
                (tags == null ? "" : "\n\nOr /reuse current tags: " + tags)
        ).withRegex("fun.MyStoreTalk::storeValidateTags", Triggers.NOT_A_COMMAND)
         .withString("fun.MyStoreTalk::storeNoTags", "/none")
         .withStringIf("fun.MyStoreTalk::doStore", "/reuse", tags != null);
    }

    public TalkResponse storeNoTags(TalkRequest request) {
        String tags = "";
        context.set(request.getChatId(), STORE_TAGS, tags);

        return doStore(request);
    }

    public TalkResponse storeValidateTags(TalkRequest request) {
        String rawTags = request.getMessage().text();
        Set<String> tags = Arrays.stream(rawTags.split(","))
            .map(storesUtils::normalizeTag)
            .filter(tag -> !tag.isEmpty())
            .collect(Collectors.toSet());
        boolean valid = tags.stream()
            .allMatch(this::tagIsValid);

        if (!valid) {
            return TalkResponse.ofText(
                request.getChatId(),
                "Invalid tag format. Try again."
            ).withButton("fun.MyStoreTalk::storeAskTags", "Continue");
        }

        context.set(request.getChatId(), STORE_TAGS, String.join(", ", tags));

        return doStore(request);
    }

    private boolean tagIsValid(String tag) {
        return tag.matches("[a-zA-Z0-9_]+");
    }

    public TalkResponse doStore(TalkRequest request) {
        Long boxId = context.getLong(request.getChatId(), STORE_BOX_ID);
        Long showcaseId = context.getLong(request.getChatId(), STORE_SHOWCASE_ID);
        String name = context.getString(request.getChatId(), STORE_NAME);
        String tagsStr = context.getString(request.getChatId(), STORE_TAGS);
        List<String> tags = tagsStr.isEmpty() ? emptyList() : Arrays.asList(tagsStr.split(", *"));
        String type = context.getString(request.getChatId(), STORE_TYPE);
        String expressionX = context.getString(request.getChatId(), STORE_EXPRESSION_X);
        String expressionY = context.getString(request.getChatId(), STORE_EXPRESSION_Y);
        String rangeStart = context.getString(request.getChatId(), STORE_RANGE_START);
        String rangeEnd = context.getString(request.getChatId(), STORE_RANGE_END);
        PlotStyle plotStyle = PlotStyle.fromStoresContext(context, request.getChatId());
        User user = authService.findUser(request.getChatId());

        StoredBox storedBox = null;
        try {
            storedBox = storeService.saveFun(
                boxId,
                showcaseId,
                name, tags,
                type,
                expressionX, expressionY,
                rangeStart, rangeEnd,
                plotStyle,
                user
            );

            storesUtils.clearContext(context, request);

            context.set(request.getChatId(), STORE_BOX_ID, storedBox.getId());
        } catch (Exception e) {
            return databaseError(request, e);
        }

        return TalkResponse.ofText(
            request.getChatId(),
            "Fun saved!"
        ).withButton("fun.MyStoreTalk::myStore", "☘ My Store")
         .withButton("fun.FunTalk::menu", "✏ Back to funs");
    }

    private TalkResponse databaseError(TalkRequest request, Exception e) {
        return TalkResponse.ofText(
            request.getChatId(),
            "Database error " + e.toString() + " " + e.getMessage() + ". Try again or tap /plot"
        ).withPreserveHandlers();
    }
}
