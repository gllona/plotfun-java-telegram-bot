package org.funbot.mathgraph.fun;

import org.funbot.mathgraph.Triggers;
import org.funbot.mathgraph.admin.AuthService;
import org.funbot.mathgraph.admin.User;
import org.funbot.mathgraph.plot.NotFunException;
import org.funbot.mathgraph.stores.*;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import static java.util.Collections.singleton;
import static org.funbot.mathgraph.fun.FunTalk.BOX_ID;
import static org.funbot.mathgraph.fun.FunTalk.DISPLAY_FUN_MODE_LIKE;
import static org.funbot.mathgraph.stores.StoresUtils.STORE_BOX_ID;
import static org.funbot.mathgraph.stores.StoresUtils.STORE_NAME;

public class ShowcaseTalk extends Talk {

    private static final String EXPLORE_BY = "EXPLORE_BY";
    private static final String EXPLORE_BY_LATEST = "LATEST";
    private static final String EXPLORE_BY_TAGS = "TAGS";
    private static final String EXPLORE_BY_TAGS_SELECTED_TAG = "SELECTED_TAG";
    private static final String EXPLORE_BY_LIKES = "LIKES";

    private AuthService authService;
    private ShowcaseService showcaseService;
    private StoreService storeService;
    private StoresUtils storesUtils;

    public ShowcaseTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("FunTalk");

        authService = AuthService.getInstance(talksConfig);
        storeService = StoreService.getInstance(talksConfig.getDao());
        showcaseService = ShowcaseService.getInstance(talksConfig.getDao());
        storesUtils = StoresUtils.getInstance();
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.of(
            InputHandler.of("fun.ShowcaseTalk::exploreShowcasedFuns",
                InputTrigger.ofRegex("/showcase"))
        );
    }

    // LIKE

    public TalkResponse like(TalkRequest request) {
        User user = authService.findUser(request.getChatId());

        long boxId = context.getLong(request.getChatId(), BOX_ID);
        StoredShowcase showcase = showcaseService.findShowcaseByBoxId(boxId).get();

        Optional<StoredLike> existingLikeBySameUser = showcaseService.findLike(user, showcase);

        if (existingLikeBySameUser.isPresent()) {
            return thanksForLike(request);
        }

        showcaseService.like(user, showcase);

        return thanksForLike(request);
    }

    private TalkResponse thanksForLike(TalkRequest request) {
        request.getInputAdapter().sendResponse(
            TalkResponse.ofText(
                request.getChatId(),
                "😊"
            ).withPreserveHandlers()
        );

        return TalkResponse.ofText(
            request.getChatId(),
            randomThanksForLike()
        ).withPreserveHandlers();
    }

    private String randomThanksForLike() {
        String[] thanksForLikeMessages = new String[]{
            "Big like, big fun!",
            "Thanks for liking!"
        };

        int i = new Random(System.currentTimeMillis()).nextInt(thanksForLikeMessages.length);

        return thanksForLikeMessages[i];
    }

    // EXPLORE SHOWCASED FUNS

    public TalkResponse exploreShowcasedFuns(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "Explore showcased funs"
        ).withButton("fun.ShowcaseTalk::showLatest", "Latest")
         .withButton("fun.ShowcaseTalk::showMostLiked", "⭐ Liked")
         .withButton("fun.ShowcaseTalk::exploreByTags", "Tagged")
         .withButton("intro.WelcomeTalk::menu", "🏕 Back");
    }

    public TalkResponse showLatest(TalkRequest request) {
        Optional<StoredShowcase> showcasedFun = showcaseService.findLatest();

        if (!showcasedFun.isPresent()) {
            return emptyShowcase(request);
        }

        context.set(request.getChatId(), EXPLORE_BY, EXPLORE_BY_LATEST);

        return showFun(request, showcasedFun.get());
    }

    public TalkResponse showMostLiked(TalkRequest request) {
        Optional<StoredShowcase> showcasedFun = showcaseService.findNextByLikes(context, request);

        if (!showcasedFun.isPresent()) {
            return emptyShowcase(request);
        }

        context.set(request.getChatId(), EXPLORE_BY, EXPLORE_BY_LIKES);

        return showFun(request, showcasedFun.get());
    }

    public TalkResponse exploreByTags(TalkRequest request) {
        String tagsStr = showcaseService.findMostUsedTags().stream()
            .map(StoredTag::getTag)
            .map(tag -> "/" + tag)
            .collect(Collectors.joining("\n* "));
        String tagsStrPrompt = tagsStr.isEmpty() ? "(no tags found)" : "* " + tagsStr;

        return TalkResponse.ofText(
            request.getChatId(),
            "Choose a most used tag or write down one here:\n" +
                "\n" +
                tagsStrPrompt
        ).withRegex("fun.ShowcaseTalk::doExploreByTags", Triggers.COMMAND)
         .withRegex("fun.ShowcaseTalk::doExploreByTags", Triggers.NOT_A_COMMAND);
    }

    public TalkResponse doExploreByTags(TalkRequest request) {
        String text = request.getMessage().text();
        String tag = storesUtils.normalizeTag(
            text.startsWith("/") ?
                text.substring(1) :
                text
        );

        Optional<StoredShowcase> showcasedFun = showcaseService.findLatestByTags(singleton(tag));

        if (!showcasedFun.isPresent()) {
            return emptyShowcase(request);
        }

        context.set(request.getChatId(), EXPLORE_BY, EXPLORE_BY_TAGS);
        context.set(request.getChatId(), EXPLORE_BY_TAGS_SELECTED_TAG, tag);

        return showFun(request, showcasedFun.get());
    }

    public TalkResponse showNext(TalkRequest request) {
        Long boxId = context.getLong(request.getChatId(), BOX_ID);
        Optional<StoredShowcase> lastShowcase = showcaseService.findShowcaseByBoxId(boxId);
        Optional<StoredShowcase> showcasedFun = findNext(request, lastShowcase.get().getId());

        if (!showcasedFun.isPresent()) {
            return emptyShowcase(request);
        }

        return showFun(request, showcasedFun.get());
    }

    private Optional<StoredShowcase> findNext(TalkRequest request, long showcaseId) {
        String exploreBy = context.getString(request.getChatId(), EXPLORE_BY);

        switch (exploreBy) {
            case EXPLORE_BY_LATEST:
                return showcaseService.findLatest(showcaseId);
            case EXPLORE_BY_TAGS:
                String tag = context.getString(request.getChatId(), EXPLORE_BY_TAGS_SELECTED_TAG);
                return showcaseService.findLatestByTags(singleton(tag), showcaseId);
            case EXPLORE_BY_LIKES:
                return showcaseService.findNextByLikes(context, request, showcaseId);
            default:
                throw new NotFunException("EXPLORE_BY not set");
        }
    }

    private TalkResponse showFun(TalkRequest request, StoredShowcase storedShowcase) {
        long boxId = storedShowcase.getBoxId();
        StoredBox box = storeService.findBox(boxId).get();
        StoredFun fun = box.getFun();

        storesUtils.saveInContext(context, request,
            boxId,
            box.getName(), box.getTagsDescription(),
            fun.getType(),
            fun.getExpressionX(), fun.getExpressionY(),
            box.getRangeStart(), box.getRangeEnd(),
            box.getPlotStyle());

        context.set(request.getChatId(), StoresUtils.STORE_SHOWCASE_ID, storedShowcase.getId());

        return talkForClass(FunTalk.class).freeStoredFun(
            request,
            DISPLAY_FUN_MODE_LIKE,
            box.getId(),
            box.getName(),
            box.getTagsDescription(),
            box.showcased(),
            fun.getType(),
            fun.getExpressionX(),
            fun.getExpressionY(),
            box.getRangeStart(),
            box.getRangeEnd(),
            box.getPlotStyle()
        );
    }

    private TalkResponse emptyShowcase(TalkRequest request) {
        return TalkResponse.ofText(
            request.getChatId(),
            "No more to showcase 🙃"
        ).withButton("fun.ShowcaseTalk::exploreShowcasedFuns", "Explore");
    }

    // SHOWCASE FUN

    public TalkResponse showcaseFun(TalkRequest request) {
        String funName = context.getString(request.getChatId(), STORE_NAME);

        return TalkResponse.ofText(
            request.getChatId(),
            "Are you sure to showcase " + funName + "?" +
                "\n" +
                "\nShowcased funs are not editable nor droppable, but you can make copies." +
                "\n" +
                "\nEvery showcased fun is likeable by the Community."
        ).withButton("fun.ShowcaseTalk::doShowcaseFun", "Yes!")
         .withButton("fun.MyStoreTalk::myStore", "Back to My Store");
    }

    public TalkResponse doShowcaseFun(TalkRequest request) {
        User user = authService.findUser(request.getChatId());
        Long boxId = context.getLong(request.getChatId(), STORE_BOX_ID);

        showcaseService.showcaseFun(user, boxId);

        return TalkResponse.ofText(
            request.getChatId(),
            "Done! You and the Community can like showcased funs!"
        ).withButton("fun.ShowcaseTalk::exploreShowcasedFuns", "Explore showcased funs")
         .withButton("fun.MyStoreTalk::myStore", "Back to My Store")
         .withButton("fun.FunTalk::menu", "Plot some fun");
    }
}
