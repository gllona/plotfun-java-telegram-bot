package org.funbot.mathgraph.geoloc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class GeolocMatches {

    public static class GeolocMatch {
        public String name;
        public String type;
        public Float latitude;
        public Float longitude;

        public GeolocMatch(String name, String type, String latitude, String longitude) {
            this.name = name;
            this.type = type;
            this.latitude = Float.parseFloat(latitude);
            this.longitude = Float.parseFloat(longitude);
        }
    }

    public List<GeolocMatch> locations = new ArrayList<>();

    public Optional<GeolocMatch> getSingleMatch() {
        return locations.size() != 1 ?
            Optional.empty() :
            Optional.of(locations.get(0));
    }

    public Optional<GeolocMatch> getMatch(int i) {
        return i >= 0 && i < locations.size() ?
            Optional.of(locations.get(i)) :
            Optional.empty();
    }
}
