package org.funbot.mathgraph.geoloc;

import LocationIq.ApiClient;
import LocationIq.ApiException;
import LocationIq.Configuration;
import LocationIq.auth.ApiKeyAuth;
import com.locationiq.client.api.SearchApi;
import com.locationiq.client.model.Location;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class LocationIqClient {

    public enum LocationType {
        continent,
        administrative
    }

    protected static Logger logger = LogManager.getLogger(LocationIqClient.class);

    private static String apiKey;
    private static LocationIqClient instance;

    public static void initialize(String apiKey) {
        LocationIqClient.apiKey = apiKey;
        getInstance();
    }

    public static synchronized LocationIqClient getInstance() {
        if (instance == null) {
            instance = new LocationIqClient();
        }
        return instance;
    }

    private ApiClient getApiClient() {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://eu1.locationiq.com/v1");

        // Configure API key authorization: key
        ApiKeyAuth key = (ApiKeyAuth) defaultClient.getAuthentication("key");
        key.setApiKey(apiKey);
        // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
        //key.setApiKeyPrefix("Token");
        return defaultClient;
    }

    public List<Location> forwardSearch(String text) {
        ApiClient defaultClient = getApiClient();
        SearchApi apiInstance = new SearchApi(defaultClient);
        String q = text; // String | Address to geocode
        String format = "json"; // String | Format to geocode. Only JSON supported for SDKs
        Integer normalizecity = 1; // Integer | For responses with no city value in the address section, the next available element in this order - city_district, locality, town, borough, municipality, village, hamlet, quarter, neighbourhood - from the address section will be normalized to city. Defaults to 1 for SDKs.
        Integer addressdetails = 0; // Integer | Include a breakdown of the address into elements. Defaults to 0.
        String viewbox = null; // String | The preferred area to find search results.  To restrict results to those within the viewbox, use along with the bounded option. Tuple of 4 floats. Any two corner points of the box - `max_lon,max_lat,min_lon,min_lat` or `min_lon,min_lat,max_lon,max_lat` - are accepted in any order as long as they span a real box.
        Integer bounded = 0; // Integer | Restrict the results to only items contained with the viewbox
        Integer limit = 10; // Integer | Limit the number of returned results. Default is 10.
        String acceptLanguage = "en"; // String | Preferred language order for showing search results, overrides the value specified in the Accept-Language HTTP header. Defaults to en. To use native language for the response when available, use accept-language=native
        String countrycodes = null; // String | Limit search to a list of countries.
        Integer namedetails = 0; // Integer | Include a list of alternative names in the results. These may include language variants, references, operator and brand.
        Integer dedupe = 1; // Integer | Sometimes you have several objects in OSM identifying the same place or object in reality. The simplest case is a street being split in many different OSM ways due to different characteristics. Nominatim will attempt to detect such duplicates and only return one match; this is controlled by the dedupe parameter which defaults to 1. Since the limit is, for reasons of efficiency, enforced before and not after de-duplicating, it is possible that de-duplicating leaves you with less results than requested.
        Integer extratags = 0; // Integer | Include additional information in the result if available, e.g. wikipedia link, opening hours.
        Integer statecode = 0; // Integer | Adds state or province code when available to the statecode key inside the address element. Currently supported for addresses in the USA, Canada and Australia. Defaults to 0
        Integer matchquality = 0; // Integer | Returns additional information about quality of the result in a matchquality object. Read more Defaults to 0 [0,1]
        Integer postaladdress = 0; // Integer | Returns address inside the postaladdress key, that is specifically formatted for each country. Currently supported for addresses in Germany. Defaults to 0 [0,1]

        try {
            logger.info("LocationIq: to execute query for: " + text);
            List<Location> result = apiInstance.search(q, format, normalizecity, addressdetails, viewbox, bounded, limit, acceptLanguage, countrycodes, namedetails, dedupe, extratags, statecode, matchquality, postaladdress);
            logger.info("LocationIq: query executed, request: " + text + " / response: " + result);
            return result;
        } catch (ApiException e) {
            logger.error("Exception when calling SearchApi#search: Status code " +  e.getCode() +
                    " / Reason: " + e.getResponseBody() +
                    " / Response headers: " + e.getResponseHeaders());
            //e.printStackTrace();
            return null;
        }
    }

    private void locationIqExample() {
        ApiClient defaultClient = getApiClient();

        SearchApi apiInstance = new SearchApi(defaultClient);
        String q = "Empire state building"; // String | Address to geocode
        String format = "json"; // String | Format to geocode. Only JSON supported for SDKs
        Integer normalizecity = 1; // Integer | For responses with no city value in the address section, the next available element in this order - city_district, locality, town, borough, municipality, village, hamlet, quarter, neighbourhood - from the address section will be normalized to city. Defaults to 1 for SDKs.
        Integer addressdetails = 1; // Integer | Include a breakdown of the address into elements. Defaults to 0.
        String viewbox = "-132.84908,47.69382,-70.44674,30.82531"; // String | The preferred area to find search results.  To restrict results to those within the viewbox, use along with the bounded option. Tuple of 4 floats. Any two corner points of the box - `max_lon,max_lat,min_lon,min_lat` or `min_lon,min_lat,max_lon,max_lat` - are accepted in any order as long as they span a real box.
        Integer bounded = 1; // Integer | Restrict the results to only items contained with the viewbox
        Integer limit = 10; // Integer | Limit the number of returned results. Default is 10.
        String acceptLanguage = "en"; // String | Preferred language order for showing search results, overrides the value specified in the Accept-Language HTTP header. Defaults to en. To use native language for the response when available, use accept-language=native
        String countrycodes = "us"; // String | Limit search to a list of countries.
        Integer namedetails = 1; // Integer | Include a list of alternative names in the results. These may include language variants, references, operator and brand.
        Integer dedupe = 1; // Integer | Sometimes you have several objects in OSM identifying the same place or object in reality. The simplest case is a street being split in many different OSM ways due to different characteristics. Nominatim will attempt to detect such duplicates and only return one match; this is controlled by the dedupe parameter which defaults to 1. Since the limit is, for reasons of efficiency, enforced before and not after de-duplicating, it is possible that de-duplicating leaves you with less results than requested.
        Integer extratags = 0; // Integer | Include additional information in the result if available, e.g. wikipedia link, opening hours.
        Integer statecode = 0; // Integer | Adds state or province code when available to the statecode key inside the address element. Currently supported for addresses in the USA, Canada and Australia. Defaults to 0
        Integer matchquality = 0; // Integer | Returns additional information about quality of the result in a matchquality object. Read more Defaults to 0 [0,1]
        Integer postaladdress = 0; // Integer | Returns address inside the postaladdress key, that is specifically formatted for each country. Currently supported for addresses in Germany. Defaults to 0 [0,1]
        try {
            List<Location> result = apiInstance.search(q, format, normalizecity, addressdetails, viewbox, bounded, limit, acceptLanguage, countrycodes, namedetails, dedupe, extratags, statecode, matchquality, postaladdress);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling SearchApi#search");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
