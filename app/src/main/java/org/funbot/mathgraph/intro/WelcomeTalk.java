package org.funbot.mathgraph.intro;

import java.util.List;
import java.util.Optional;
import org.funbot.mathgraph.admin.AuthService;
import org.funbot.mathgraph.geoloc.GeolocService;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.InputTrigger;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

public class WelcomeTalk extends Talk {

    private final AuthService authService = AuthService.getInstance(talksConfig);
    private final GeolocService geolocService = GeolocService.getInstance(talksConfig);

    public WelcomeTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("FunTalk");
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.of(
            InputHandler.of("intro.WelcomeTalk::start",
                InputTrigger.ofRegex("/start( .*)?"))
        );
    }

    public TalkResponse start(TalkRequest request) {
        Optional<TalkResponse> initialization = authService.firstTimeRoute(request.getChatId());
        if (initialization.isPresent()) {
            return initialization.get();
        }

        authService.saveUser(request);
        authService.fillInUsers(request);

        List<String> matches = request.getMatch().getRegexMatches();
        String arguments = matches.size() == 1 ? null : matches.get(1);

        if (arguments != null) {
            return startWithArguments(request, arguments);
        }

        resetContext(request);

        return menu(request);
    }

    public TalkResponse menu(TalkRequest request) {
        return TalkResponse.ofHtml(
            request.getChatId(),
            "<b>Have Fun</b>"
        ).withButton("fun.FunTalk::menu", "✏️ Plot")
         .withButton("fun.ShowcaseTalk::exploreShowcasedFuns", "🚴 Showcase")
         .withButton("fun.MyStoreTalk::myStore", "☘ My Store")
         .withButton("intro.WelcomeTalk::about", "ℹ About plotfun");
    }

    public TalkResponse about(TalkRequest request) {
        return TalkResponse.ofHtml(
            request.getChatId(),
            "<b>plotfun</b> is a Telegram bot to have fun with funs!\n" +
                "\n" +
                "It is an educational project also. Source code is available <a href=\"https://gitlab.com/gllona/plotfun-java-telegram-bot\">here</a>\n" +
                "\n" +
                "Stack: Raspberry PI 4, Linux, MariaDB, Java, Talks Telegram chatbot framework\n" +
                "\n" +
                "Licensed under GNU GPL\n" +
                "\n" +
                "Built by <a href=\"http://desarrolladores.logicos.org/gorka/\">Gorka Llona</a> and friends\n" +
                "\n" +
                "Tap on /answer to drop us a note\n" +
                "\n" +
                "Have fun!"
        ).withButton("intro.WelcomeTalk::menu", "Continue");
    }

    private TalkResponse startWithArguments(TalkRequest request, String arguments) {
        return TalkResponse.ofText(
            request.getChatId(),
            "/start with arguments is not supported yet"
        );
    }

    private void resetContext(TalkRequest request) {
        talksConfig.getTalkManager().resetTalkContexts(request.getChatId());
    }
}
