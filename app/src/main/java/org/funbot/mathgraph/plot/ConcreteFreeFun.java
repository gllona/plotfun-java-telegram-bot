package org.funbot.mathgraph.plot;

import gnu.jel.CompiledExpression;
import java.util.function.Function;

public class ConcreteFreeFun implements FreeFun {

    protected String xExpression;
    protected String yExpression;

    protected CompiledExpression compiledXexpression;
    protected CompiledExpression compiledYexpression;

    protected final JelProxy jel;
    protected final IndependentVariable independentVariable;

    public ConcreteFreeFun(JelProxy jelProxy, IndependentVariable independentVariable) {
        this.jel = jelProxy;
        this.independentVariable = independentVariable;
    }

    @Override
    public String getXexpression() {
        return xExpression;
    }

    @Override
    public String getYexpression() {
        return yExpression;
    }

    @Override
    public void setXexpression(String xExpression) {
        this.xExpression = xExpression;
    }

    @Override
    public void setYexpression(String yExpression) {
        this.yExpression = yExpression;
    }

    @Override
    public void setXandCompile(String xExpression) {
        setXexpression(xExpression);
        setCompiledXexpression(
            jel.compile(xExpression)
        );
    }

    @Override
    public void setYandCompile(String yExpression) {
        setYexpression(yExpression);
        setCompiledYexpression(
            jel.compile(yExpression)
        );
    }

    @Override
    public void setCompiledXexpression(CompiledExpression compiledXexpression) {
        this.compiledXexpression = compiledXexpression;
    }

    @Override
    public void setCompiledYexpression(CompiledExpression compiledYexpression) {
        this.compiledYexpression = compiledYexpression;
    }

    @Override
    public CompiledExpression getCompiledXexpression() {
        return compiledXexpression;
    }

    @Override
    public CompiledExpression getCompiledYexpression() {
        return compiledYexpression;
    }

    @Override
    public JelProxy getJel() {
        return jel;
    }

    @Override
    public boolean isXYP() {
        return IndependentVariable.P == independentVariable;
    }

    @Override
    public boolean isXY() {
        return IndependentVariable.X == independentVariable;
    }

    @Override
    public Function<Double, Double> x() {
        if (compiledXexpression == null) {
            throw new FunException("X expression is not compiled");
        }

        return p -> jel.eval(compiledXexpression, p);
    }

    @Override
    public Function<Double, Double> y() {
        if (compiledYexpression == null) {
            throw new FunException("Y expression is not compiled");
        }

        return p -> jel.eval(compiledYexpression, p);
    }
}
