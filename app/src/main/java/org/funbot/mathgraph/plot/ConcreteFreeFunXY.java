package org.funbot.mathgraph.plot;

import gnu.jel.CompiledExpression;
import java.util.function.Function;

public class ConcreteFreeFunXY extends ConcreteFreeFun implements FunXY {

    private static final String X_IS_P_EXPR = "p";

    public ConcreteFreeFunXY(JelProxy jelProxy) {
        super(jelProxy, IndependentVariable.X);
        super.setXexpression(X_IS_P_EXPR);
    }

    @Override
    public Function<Double, Double> x() {
        return Fun.X_IS_P;
    }

    @Override
    public void setXexpression(String xExpression) {
        // fun is fixed
    }

    @Override
    public void setXandCompile(String xExpression) {
        // fixed
    }

    @Override
    public void setCompiledXexpression(CompiledExpression compiledXexpression) {
        // fixed
    }

    @Override
    public String toString() {
        return "y = " + yExpression;
    }
}
