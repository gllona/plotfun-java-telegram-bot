package org.funbot.mathgraph.plot;

public class ConcreteFreeFunXYP extends ConcreteFreeFun implements FunXYP {

    public ConcreteFreeFunXYP(JelProxy jelProxy) {
        super(jelProxy, IndependentVariable.P);
    }

    @Override
    public String toString() {
        return "x = " + xExpression + "\n" +
            "y = " + yExpression;
    }
}
