package org.funbot.mathgraph.plot;

import gnu.jel.CompiledExpression;

public interface FreeFun extends Fun {

    String getXexpression();
    String getYexpression();

    void setXexpression(String xExpression);
    void setYexpression(String yExpression);

    void setXandCompile(String xExpression);
    void setYandCompile(String yExpression);

    void setCompiledXexpression(CompiledExpression compiledXexpression);
    void setCompiledYexpression(CompiledExpression compiledYexpression);

    CompiledExpression getCompiledXexpression();
    CompiledExpression getCompiledYexpression();

    JelProxy getJel();

    boolean isXYP();
    boolean isXY();
}
