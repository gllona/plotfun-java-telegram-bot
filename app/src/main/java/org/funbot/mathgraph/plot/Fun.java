package org.funbot.mathgraph.plot;

import java.util.function.Function;

public interface Fun {

    Function<Double, Double> X_IS_P = p -> p;
    Function<Double, Double> Y_IS_P = p -> p;

    Function<Double, Double> X_IS_0 = p -> 0.0;
    Function<Double, Double> Y_IS_0 = p -> 0.0;

    Function<Double, Double> x();

    Function<Double, Double> y();

    default String type() {
        return this instanceof FunXY ? "XY" : "XYP";
    }
}
