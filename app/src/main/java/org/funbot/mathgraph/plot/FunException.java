package org.funbot.mathgraph.plot;

public class FunException extends RuntimeException {

    public FunException(String message) {
        super(message);
    }

    public FunException(String message, Throwable cause) {
        super(message, cause);
    }
}
