package org.funbot.mathgraph.plot;

import java.util.function.Function;

public interface FunXY extends Fun {

    @Override
    default Function<Double, Double> x() {
        return Fun.X_IS_P;
    }
}
