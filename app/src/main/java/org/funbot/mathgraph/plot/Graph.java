package org.funbot.mathgraph.plot;

import java.io.File;

public class Graph {

    double p0;
    double p1;

    PlotStyle style;

    public File file;

    boolean calc = false;

    Window window;

    public Graph(int width, int height, double p0, double p1, PlotStyle style, Fun fun) {
        this.p0 = p0;
        this.p1 = p1;

        this.style = style;

        this.window = new Window();

        window.portWidth = width;
        window.portHeight = height;

        window.x0 = fun.x().apply(p0);
        window.y0 = fun.y().apply(p0);

        window.x1 = fun.x().apply(p1);
        window.y1 = fun.y().apply(p1);
    }

    double size() {
        return p1 - p0;
    }

    static class Window {

        double x0;
        double y0;

        double x1;
        double y1;

        double minX;
        double maxX;

        double minY;
        double maxY;

        int maxPoint;
        double[] xs;
        double[] ys;

        int portWidth;
        int portHeight;

        Double width() {
            return x1 - x0;
        }

        Double height() {
            return y1 - y0;
        }

        Double minMaxWidth() {
            return maxX - minX;
        }

        Double minMaxHeight() {
            return maxY - minY;
        }
    }
}
