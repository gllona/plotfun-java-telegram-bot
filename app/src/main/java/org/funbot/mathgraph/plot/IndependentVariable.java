package org.funbot.mathgraph.plot;

public enum IndependentVariable {
    P,
    X
}
