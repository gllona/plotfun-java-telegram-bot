package org.funbot.mathgraph.plot;

import java.util.function.Function;

public interface Invertible {

  Function<Double, Double> invX();

  Function<Double, Double> invY();
}
