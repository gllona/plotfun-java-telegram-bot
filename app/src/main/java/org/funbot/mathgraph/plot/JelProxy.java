package org.funbot.mathgraph.plot;

import gnu.jel.CompilationException;
import gnu.jel.CompiledExpression;
import gnu.jel.DVMap;
import gnu.jel.Evaluator;
import gnu.jel.Library;

public class JelProxy {

    public static class ParameterProviderP {
        public Double pVar;
        public Double p() {
            return pVar;
        }
        public Double getDoubleProperty(String name) {
            if ("p".equals(name)) {
                return p();
            }
            return null;
        }
    }

    public static class ParameterProviderX {
        public Double xVar;
        public Double x() {
            return xVar;
        }
        public Double getDoubleProperty(String name) {
            if ("x".equals(name)) {
                return x();
            }
            return null;
        }
    }

    private ParameterProviderP parameterProviderP = new ParameterProviderP();
    private ParameterProviderX parameterProviderX = new ParameterProviderX();
    private Object[] contextP = new Object[]{ parameterProviderP };
    private Object[] contextX = new Object[]{ parameterProviderX };

    private boolean isXYP;

    public JelProxy(IndependentVariable independentVariable) {
        this.isXYP = IndependentVariable.P == independentVariable;
    }

    public synchronized CompiledExpression compile(String expr) {
        Class<?>[] staticLib = new Class<?>[1];
        try {
            staticLib[0] = Class.forName("java.lang.Math");
        } catch (ClassNotFoundException ignored) {}

        Class[] dynamicLibs = new Class[1];
        if (isXYP) {
            dynamicLibs[0] = parameterProviderP.getClass();
        } else {
            dynamicLibs[0] = parameterProviderX.getClass();
        }

        DVMap resolver = new DVMap() {
            @Override
            public String getTypeName(String s) {
                if ("p".equals(s) && isXYP || "x".equals(s) && !isXYP) {
                    return "Double";
                }
                return null;
            }
        };

        Library lib = new Library(staticLib, dynamicLibs, null, resolver, null);
        try {
            lib.markStateDependent("random", null);
        } catch (CompilationException ignored) {}

        CompiledExpression compiledExpression = null;
        try {
            compiledExpression = Evaluator.compile(expr, lib);
        } catch (CompilationException ce) {
            StringBuilder sb = new StringBuilder();
            sb.append("COMPILER ERROR :").append("\n");
            sb.append(ce.getMessage()).append("\n");
            sb.append("    ").append(expr);
            //sb.append("\n");
            //int column = ce.getColumn();
            //for (int i = 0; i < column + 4 - 1; i++) {
            //    sb.append(' ');
            //}
            //sb.append('^');
            String message = sb.toString();

            throw new FunException(message);
        }

        return compiledExpression;
    }

    public synchronized Double eval(CompiledExpression expression, Double parameter) {
        Object[] context;
        if (isXYP) {
            context = this.contextP;
            parameterProviderP.pVar = parameter;
        } else {
            context = this.contextX;
            parameterProviderX.xVar = parameter;
        }

        Object result = null;
        try {
            result = expression.evaluate(context);

            if (result == null) {
                throw new FunException("Expression evaluation returned null");
            }

            try {
                return Double.valueOf(result.toString());
            } catch (NumberFormatException e) {
                throw new FunException("Expression evaluation is not a number: " + result);
            }

        } catch (ArithmeticException e) {
            return Double.NaN;

        } catch (Throwable e) {
            throw new FunException("Error at expression evaluation: " + e.getMessage(), e);
        }
    }
}
