package org.funbot.mathgraph.plot;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public enum LineStyle {
    SOLID("Solid"),
    SHORT_DASH("Short dash"),
    LONG_DASH("Long dash");

    @Getter
    private final String name;

    public static LineStyle from(String value) {
        try {
            return valueOf(value);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    public static LineStyle fromName(String name) {
        return Stream.of(values())
            .filter(lineStyle -> lineStyle.name.equals(name))
            .findFirst()
            .orElse(null);
    }
}
