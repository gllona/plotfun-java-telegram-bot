package org.funbot.mathgraph.plot;

public class NotFunException extends RuntimeException {

    public NotFunException(String message) {
        super(message);
    }

    public NotFunException(String message, Throwable cause) {
        super(message, cause);
    }
}
