package org.funbot.mathgraph.plot;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.funbot.mathgraph.fun.FunTalk;
import org.funbot.mathgraph.stores.StoresUtils;
import org.logicas.librerias.talks.engine.TalkContext;

@RequiredArgsConstructor
@Getter
public class PlotStyle {

    private final boolean showAxes;
    private final String lineColorRgbCode;
    private final int thickness;
    private final LineStyle lineStyle;
    private final String backgroundColorRgbCode;

    public static PlotStyle buildWithDefaultValues(
        Boolean showAxes,
        String lineColorRgbCode,
        Integer thickness,
        String lineStyle,
        String backgroundColorRgbCode
    ) {
        return new PlotStyle(
            showAxes == null || showAxes,
            lineColorRgbCode == null ? PlotterColor.Black.getRgbCode() : lineColorRgbCode,
            thickness == null ? 1 : thickness,
            lineStyle == null ? LineStyle.SOLID : LineStyle.from(lineStyle),
            backgroundColorRgbCode == null ? PlotterColor.White.getRgbCode() : backgroundColorRgbCode
        );
    }

    public static PlotStyle fromStoresContext(TalkContext context, long chatId) {
        Integer showAxes = context.getInt(chatId, StoresUtils.STORE_DISPLAY_AXES);
        String lineColorRgbCode = context.getString(chatId, StoresUtils.STORE_LINE_COLOR);
        Integer thickness = context.getInt(chatId, StoresUtils.STORE_LINE_THICKNESS);
        String lineStyle = context.getString(chatId, StoresUtils.STORE_LINE_STYLE);
        String backgroundColorRgbCode = context.getString(chatId, StoresUtils.STORE_BACKGROUND_COLOR);

        return buildWithDefaultValues(
            showAxes == null || showAxes == 1,
            lineColorRgbCode,
            thickness,
            lineStyle,
            backgroundColorRgbCode);
    }

    public static void saveInStoresContext(TalkContext context, long chatId, PlotStyle plotStyle) {
        context.set(chatId, StoresUtils.STORE_DISPLAY_AXES, plotStyle.isShowAxes() ? 1 : 0);
        context.set(chatId, StoresUtils.STORE_LINE_COLOR, plotStyle.getLineColorRgbCode());
        context.set(chatId, StoresUtils.STORE_LINE_THICKNESS, plotStyle.getThickness());
        context.set(chatId, StoresUtils.STORE_LINE_STYLE, plotStyle.getLineStyle().name());
        context.set(chatId, StoresUtils.STORE_BACKGROUND_COLOR, plotStyle.getBackgroundColorRgbCode());
    }

    public static void clearStoresContext(TalkContext context, long chatId) {
        context.remove(chatId, StoresUtils.STORE_DISPLAY_AXES);
        context.remove(chatId, StoresUtils.STORE_LINE_COLOR);
        context.remove(chatId, StoresUtils.STORE_LINE_THICKNESS);
        context.remove(chatId, StoresUtils.STORE_LINE_STYLE);
        context.remove(chatId, StoresUtils.STORE_BACKGROUND_COLOR);
    }

    public static PlotStyle fromFunContext(TalkContext context, long chatId) {
        Integer showAxes = context.getInt(chatId, FunTalk.DISPLAY_AXES);
        String lineColorRgbCode = context.getString(chatId, FunTalk.LINE_COLOR);
        Integer thickness = context.getInt(chatId, FunTalk.LINE_THICKNESS);
        String lineStyle = context.getString(chatId, FunTalk.LINE_STYLE);
        String backgroundColorRgbCode = context.getString(chatId, FunTalk.BACKGROUND_COLOR);

        return buildWithDefaultValues(
            showAxes == null || showAxes == 1,
            lineColorRgbCode,
            thickness,
            lineStyle,
            backgroundColorRgbCode);
    }

    public static void saveInFunContext(TalkContext context, long chatId, PlotStyle plotStyle) {
        context.set(chatId, FunTalk.DISPLAY_AXES, plotStyle.isShowAxes() ? 1 : 0);
        context.set(chatId, FunTalk.LINE_COLOR, plotStyle.getLineColorRgbCode());
        context.set(chatId, FunTalk.LINE_THICKNESS, plotStyle.getThickness());
        context.set(chatId, FunTalk.LINE_STYLE, plotStyle.getLineStyle().name());
        context.set(chatId, FunTalk.BACKGROUND_COLOR, plotStyle.getBackgroundColorRgbCode());
    }
}
