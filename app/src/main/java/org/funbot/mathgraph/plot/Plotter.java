package org.funbot.mathgraph.plot;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static java.lang.Double.isNaN;

/* */
public class Plotter {

    public static final int PLOT_SIZE = 400;
    public static final int FULL_DASH_COUNT = PLOT_SIZE / 5;

    private static Logger logger = LogManager.getLogger(Plotter.class);

    private static Plotter instance = null;

    public static synchronized Plotter getInstance() {
        if (instance == null) {
            instance = new Plotter();
        }
        return instance;
    }

    public Graph plot(Graph graph, Fun fun) {

        BufferedImage image = createImage(graph.window.portWidth, graph.window.portHeight);
        Graphics2D graphics = createGraphics(graph, image);

        calcPlotFun(graphics, graph, fun); // optional step needed for window adjustment

        graphics.setPaint(PlotterColor.toAwtColor(graph.style.getBackgroundColorRgbCode()));
        graphics.fill(new Rectangle2D.Double(0, 0, graph.window.maxPoint, graph.window.maxPoint));

        if (graph.style.isShowAxes()) {
            graphics.setColor(Color.LIGHT_GRAY);

            doPlotFun(graphics, graph, new XAxis());
            doPlotFun(graphics, graph, new YAxis());
        }

        graphics.setColor(PlotterColor.toAwtColor(graph.style.getLineColorRgbCode()));
        graphics.setStroke(buildStroke(graph.style));

        drawPlotFun(graphics, graph, fun);

        try {
            File plotPng = File.createTempFile("plot_", ".png");
            ImageIO.write(image, "PNG", plotPng);
            graph.file = plotPng;

            //logger.info("Saved image in " + graph.file.getPath());

            return graph;

        } catch (IOException e) {
            throw new NotFunException("Can not create PNG", e);
        }
    }

    private Stroke buildStroke(PlotStyle style) {
        int dashSize = PLOT_SIZE / FULL_DASH_COUNT;
        float[] dash = { PLOT_SIZE, 0.0f }; // SOLID

        switch (style.getLineStyle()) {
            case SHORT_DASH:
                dash = new float[]{ dashSize, dashSize };
                break;
            case LONG_DASH:
                dash = new float[]{ 4 * dashSize, dashSize };
                break;
        }

        BasicStroke stroke = new BasicStroke(
            style.getThickness(),
            BasicStroke.CAP_ROUND,
            BasicStroke.JOIN_ROUND,
            1.0f, dash, 0.0f
        );
        return stroke;
    }

    private void calcPlotFun(Graphics2D graphics, Graph graph, Fun fun) {
        double step = getStep(graph);
        int numPoints = Math.max(graph.window.portWidth, graph.window.portHeight) + 10;

        double[] xs = new double[numPoints];
        double[] ys = new double[numPoints];

        int point = 0;
        double p = graph.p0;
        Double x = fun.x().apply(p);
        Double y = fun.y().apply(p);

        xs[point] = x;
        ys[point] = y;

        graph.window.minX = x;
        graph.window.maxX = x;
        graph.window.minY = y;
        graph.window.maxY = y;

        while (p <= graph.p1) {
            point++;

            p += step;
            x = fun.x().apply(p);
            y = fun.y().apply(p);

            xs[point] = x;
            ys[point] = y;

            if (isNaN(graph.window.minX) || x < graph.window.minX) {
                graph.window.minX = x;
            }
            if (isNaN(graph.window.maxX) || x > graph.window.maxX) {
                graph.window.maxX = x;
            }
            if (isNaN(graph.window.minY) || y < graph.window.minY) {
                graph.window.minY = y;
            }
            if (isNaN(graph.window.maxY) || y > graph.window.maxY) {
                graph.window.maxY = y;
            }
        }

        if (isNaN(graph.window.minX)) {
            graph.window.minX = -1.0;
        }
        if (isNaN(graph.window.maxX)) {
            graph.window.maxX = 1.0;
        }
        if (isNaN(graph.window.minY)) {
            graph.window.minY = -1.0;
        }
        if (isNaN(graph.window.maxY)) {
            graph.window.maxY = 1.0;
        }

        graph.window.maxPoint = point;
        graph.window.xs = xs;
        graph.window.ys = ys;

        double extraSideWidth = graph.window.minMaxWidth() * 0.1;
        double extraSideHeight = graph.window.minMaxHeight() * 0.1;

        if (isNaN(graph.window.x0) || graph.window.minX - extraSideWidth < graph.window.x0) {
            graph.window.x0 = graph.window.minX - extraSideWidth;
        }
        if (isNaN(graph.window.x1) || graph.window.maxX + extraSideWidth > graph.window.x1) {
            graph.window.x1 = graph.window.maxX + extraSideWidth;
        }
        if (isNaN(graph.window.y0) || graph.window.minY - extraSideHeight < graph.window.y0) {
            graph.window.y0 = graph.window.minY - extraSideHeight;
        }
        if (isNaN(graph.window.y1) || graph.window.maxY + extraSideHeight > graph.window.y1) {
            graph.window.y1 = graph.window.maxY + extraSideHeight;
        }

        if (graph.window.width() > graph.window.height()) {
            double diffXY = graph.window.width() - graph.window.height();
            graph.window.y0 -= diffXY / 2.0;
            graph.window.y1 += diffXY / 2.0;
        }
        if (graph.window.height() > graph.window.width()) {
            double diffXY = graph.window.height() - graph.window.width();
            graph.window.x0 -= diffXY / 2.0;
            graph.window.x1 += diffXY / 2.0;
        }

        //graph.calc = true;
    }

    private void plotFun(Graphics2D graphics, Graph graph, Fun fun) {
        if (!graph.calc) {
            doPlotFun(graphics, graph, fun);
        } else {
            drawPlotFun(graphics, graph, fun);
        }
    }

    private void doPlotFun(Graphics2D graphics, Graph graph, Fun fun) {
        double p0;
        double p1;

        if (fun instanceof Invertible) {
            Invertible invertible = (Invertible) fun;
            double p0fromX = invertible.invX().apply(graph.window.x0);
            double p0fromY = invertible.invY().apply(graph.window.y0);
            double p1fromX = invertible.invX().apply(graph.window.x1);
            double p1fromY = invertible.invY().apply(graph.window.y1);
            p0 = minOrDefault(p0fromX, p0fromY, graph.p0);
            p1 = maxOrDefault(p1fromX, p1fromY, graph.p1);
        } else {
            p0 = graph.p0;
            p1 = graph.p1;
        }

        double step = getStep(graph, p0, p1);
        double p = p0;

        Double x = fun.x().apply(p);
        Double y = fun.y().apply(p);

        Integer portX = toPortX(graph, x);
        Integer portY = toPortY(graph, y);

        int dashScale = getDashScale(graph.window.maxPoint);
        int strokeCycle = getStrokeCycle(graph.style, dashScale);
        int strokeCount = dashScale;

        while (p <= p1) {
            Integer lastPortX = portX;
            Integer lastPortY = portY;

            p += step;
            x = fun.x().apply(p);
            y = fun.y().apply(p);

            portX = toPortX(graph, x);
            portY = toPortY(graph, y);

            strokeCount = (strokeCount + 1) % strokeCycle;
            if (strokeCount / dashScale == 0) {
                continue;
            }

            if (lastPortX != null && lastPortY != null && portX != null && portY != null) {
                graphics.drawLine(lastPortX, lastPortY, portX, portY);
            }
        }
    }

    private void drawPlotFun(Graphics2D graphics, Graph graph, Fun fun) {
        int point = 0;
        Integer portX = toPortX(graph, graph.window.xs[point]);
        Integer portY = toPortY(graph, graph.window.ys[point]);

        int dashScale = getDashScale(graph.window.maxPoint);
        int strokeCycle = getStrokeCycle(graph.style, dashScale);
        int strokeCount = dashScale;

        while (point++ < graph.window.maxPoint) {

            Integer lastPortX = portX;
            Integer lastPortY = portY;

            portX = toPortX(graph, graph.window.xs[point]);
            portY = toPortY(graph, graph.window.ys[point]);

            strokeCount = (strokeCount + 1) % strokeCycle;
            if (strokeCount / dashScale == 0) {
                continue;
            }

            if (lastPortX != null && lastPortY != null && portX != null && portY != null) {
                graphics.drawLine(lastPortX, lastPortY, portX, portY);
            }
        }
    }

    private int getDashScale(int maxPoint) {
        return maxPoint / FULL_DASH_COUNT;
    }

    private int getStrokeCycle(PlotStyle style, int dashScale) {
        switch (style.getLineStyle()) {
            case SOLID:
                return Integer.MAX_VALUE;
            case SHORT_DASH:
                return 2 * dashScale;
            case LONG_DASH:
                return 4 * dashScale;
        }

        throw new NotFunException("LineStyle not set");
    }

    private double getStep(Graph graph) {
        double step = graph.size() / Math.max(graph.window.portWidth, graph.window.portHeight);
        return step != 0 ? step : 1e-4;
    }

    private double getStep(Graph graph, double p0, double p1) {
        double step = (p1 - p0) / Math.max(graph.window.portWidth, graph.window.portHeight);
        return step != 0 ? step : 1e-4;
    }

    private Integer toPortX(Graph graph, Double x) {
        return isNaN(x) ?
            null :
            (int) (
                ((x - graph.window.x0) / graph.window.width()) * graph.window.portWidth + 0.5
            );
    }

    private Integer toPortY(Graph graph, Double y) {
        return isNaN(y) ?
            null :
            (int) (
                ((graph.window.y1 - y) / graph.window.height()) * graph.window.portHeight + 0.5
        );
    }

    private double minOrDefault(double v1, double v2, double vDefault) {
        if (isNaN(v1)) {
            return isNaN(v2) ? vDefault : v2;
        } else if (isNaN(v2)) {
            return v1;
        } else {
            return Math.min(v1, v2);
        }
    }

    private double maxOrDefault(double v1, double v2, double vDefault) {
        if (isNaN(v1)) {
            return isNaN(v2) ? vDefault : v2;
        } else if (isNaN(v2)) {
            return v1;
        } else {
            return Math.max(v1, v2);
        }
    }

    private BufferedImage createImage(int width, int height) {
        return new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    }

    private Graphics2D createGraphics(Graph graph, BufferedImage image) {
        return image.createGraphics();
    }
}
