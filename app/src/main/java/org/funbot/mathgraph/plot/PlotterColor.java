package org.funbot.mathgraph.plot;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.awt.*;

@RequiredArgsConstructor
public enum PlotterColor {

    Black("#000000"),
    White("#FFFFFF"),
    Red("#FF0000"),
    Lime("#00FF00"),
    Blue("#0000FF"),
    Yellow("#FFFF00"),
    Cyan("#00FFFF"),
    Magenta("#FF00FF"),
    Silver("#C0C0C0"),
    Gray("#808080"),
    Maroon("#800000"),
    Olive("#808000"),
    Green("#008000"),
    Purple("#800080"),
    Teal("#008080"),
    Navy("#000080");

    @Getter
    private final String rgbCode;

    public static PlotterColor from(String rgbCode) {
        try {
            return PlotterColor.valueOf(rgbCode);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    public Color toAwtColor() {
        return toAwtColor(rgbCode);
    }

    public static Color toAwtColor(String rgbCode) {
        int red = Integer.parseInt(rgbCode.substring(1, 3), 16);
        int green = Integer.parseInt(rgbCode.substring(3, 5), 16);
        int blue = Integer.parseInt(rgbCode.substring(5, 7), 16);

        return new Color(red, green, blue);
    }
}
