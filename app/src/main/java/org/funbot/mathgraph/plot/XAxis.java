package org.funbot.mathgraph.plot;

import static java.lang.Double.NaN;

import java.util.function.Function;

public class XAxis implements Fun, Invertible {

    @Override
    public Function<Double, Double> x() {
        return Fun.X_IS_P;
    }

    @Override
    public Function<Double, Double> y() {
        return Fun.Y_IS_0;
    }

    @Override
    public Function<Double, Double> invX() {
        return x -> x;
    }

    @Override
    public Function<Double, Double> invY() {
        return y -> NaN;
    }
}
