package org.funbot.mathgraph.stores;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.funbot.mathgraph.AppSetting;
import org.funbot.mathgraph.CustomThreadFactory;
import org.logicas.librerias.copersistance.Dao;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ShowcaseCache {

    @RequiredArgsConstructor
    @Getter
    @EqualsAndHashCode
    private class CacheKey {
        private final Long showcaseId;
    }

    private class Cacher implements Runnable {
        private boolean doFinish = false;
        private int lastLikeCount;

        @Override
        public void run() {
            while (! doFinish && ! Thread.currentThread().isInterrupted()) {
                cacheFuns();
            }
        }

        private void cacheFuns() {
            cacheLatest();
            cacheLiked();

            try {
                Thread.sleep(CACHE_SLEEP_MILLISECS);
            } catch (InterruptedException e) {
                cancel();
            }
        }

        private void cacheLatest() {
            Optional<StoredShowcase> latest = showcaseGenerator.findLatest();

            Map<CacheKey, Optional<StoredShowcase>> newCache = new ConcurrentHashMap<>();
            CacheKey key = new CacheKey(null);
            newCache.put(key, latest);

            for (int cacheSize = 1; latest.isPresent() && cacheSize <= CACHE_SIZE_LATEST; cacheSize++) {
                long beforeShowcaseId = latest.get().getId();
                latest = showcaseGenerator.findLatest(beforeShowcaseId);

                key = new CacheKey(beforeShowcaseId);
                newCache.put(key, latest);
            }

            replaceLatestCache(newCache);
        }

        private void cacheLiked() {
            Optional<StoredShowcase> latest = showcaseGenerator.findNextByLikes(
                null,
                this::getLastLikeCount,
                this::setLastLikeCount
            );

            Map<CacheKey, Optional<StoredShowcase>> newCache = new ConcurrentHashMap<>();
            CacheKey key = new CacheKey(null);
            newCache.put(key, latest);

            for (int cacheSize = 1; latest.isPresent() && cacheSize <= CACHE_SIZE_LIKED; cacheSize++) {
                long lastShowcaseIdShown = latest.get().getId();
                latest = showcaseGenerator.findNextByLikes(
                    lastShowcaseIdShown,
                    this::getLastLikeCount,
                    this::setLastLikeCount
                );

                key = new CacheKey(lastShowcaseIdShown);
                newCache.put(key, latest);
            }

            replaceLikedCache(newCache);
        }

        public int getLastLikeCount() {
            return lastLikeCount;
        }

        public void setLastLikeCount(int count) {
            lastLikeCount = count;
        }

        public synchronized void cancel() {
            doFinish = true;
        }
    }

    private static final int CACHE_SIZE_LATEST = 50;
    private static final int CACHE_SIZE_LIKED = 50;
    private static final int CACHE_SLEEP_MILLISECS = 10000;
    private static final String THREADS_ID = "FUNCACHE";
    private static ShowcaseCache instance = null;
    private static final Logger logger = LogManager.getLogger(ShowcaseCache.class);

    private final ShowcaseGenerator showcaseGenerator;
    private final CustomThreadFactory threadFactory = CustomThreadFactory.getInstance();
    private ExecutorService executor;
    private Cacher cacher;
    private Map<CacheKey, Optional<StoredShowcase>> latestCache = new ConcurrentHashMap<>();
    private Map<CacheKey, Optional<StoredShowcase>> likedCache = new ConcurrentHashMap<>();

    public static synchronized ShowcaseCache getInstance(Dao dao) {
        if (instance == null) {
            instance = new ShowcaseCache(dao);
        }
        return instance;
    }

    private ShowcaseCache(Dao dao) {
        showcaseGenerator = ShowcaseGenerator.getInstance(dao);

        createExecutor();
        cacher = new Cacher();
        executor.execute(cacher);
    }

    private void createExecutor() {
        executor = Executors.newSingleThreadExecutor(
            threadFactory.create(THREADS_ID)
        );
    }

    public Optional<StoredShowcase> findLatest(Long beforeShowcaseId) {
        CacheKey key = new CacheKey(beforeShowcaseId);
        return getLatestCache().get(key);
    }

    public Optional<StoredShowcase> findNextByLikes(Long lastShowcaseIdShown) {
        CacheKey key = new CacheKey(lastShowcaseIdShown);
        return getLikedCache().get(key);
    }

    private synchronized Map<CacheKey, Optional<StoredShowcase>> getLatestCache() {
        return latestCache;
    }

    private synchronized void replaceLatestCache(Map<CacheKey, Optional<StoredShowcase>> newCache) {
        latestCache = newCache;
    }

    private synchronized Map<CacheKey, Optional<StoredShowcase>> getLikedCache() {
        return likedCache;
    }

    private synchronized void replaceLikedCache(Map<CacheKey, Optional<StoredShowcase>> newCache) {
        likedCache = newCache;
    }

    public void shutdown() {
        logger.info("Stopping funcache...");
        threadFactory.interruptThreads(THREADS_ID);
        logger.info("Shutting down executor...");
        executor.shutdown();
        try {
            executor.awaitTermination(AppSetting.getInteger(AppSetting.SHUTDOWN_TIMEOUT_IN_MILLIS), TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            logger.warn("Interrupted while waiting for StoredShowcaseCache to finish");
        }
        logger.info("funcache stopped.");
    }
}
