package org.funbot.mathgraph.stores;

import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.talks.engine.TalkContext;
import org.logicas.librerias.talks.engine.TalkRequest;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Collections.reverseOrder;
import static java.util.Comparator.comparingLong;
import static java.util.Optional.ofNullable;

public class ShowcaseGenerator {

    private static final String EXPLORE_BY_LIKES_LAST_LIKE_COUNT = "LAST_LIKE_COUNT";

    private static ShowcaseGenerator instance = null;

    private StoredShowcaseRepository storedShowcaseRepository;
    private StoredTagRepository storedTagRepository;
    private StoredBoxTagRepository storedBoxTagRepository;
    private StoresUtils storesUtils = StoresUtils.getInstance();

    public static synchronized ShowcaseGenerator getInstance(Dao dao) {
        if (instance == null) {
            instance = new ShowcaseGenerator(dao);
        }
        return instance;
    }

    public ShowcaseGenerator(Dao dao) {
        storedShowcaseRepository = StoredShowcaseRepository.getInstance(dao);
        storedTagRepository = StoredTagRepository.getInstance(dao);
        storedBoxTagRepository = StoredBoxTagRepository.getInstance(dao);
    }

    public Optional<StoredShowcase> findShowcase(long showcaseId) {
        return ofNullable(
            storedShowcaseRepository.readById(showcaseId)
        );
    }

    public Optional<StoredShowcase> findShowcaseByBoxId(long boxId) {
        return storedShowcaseRepository.readByField(StoredShowcase.FIELD_BOX_ID, boxId)
            .stream()
            .findFirst();
    }

    public Optional<StoredShowcase> findLatest() {
        return ofNullable(
            storedShowcaseRepository.readLatest()
        );
    }

    public Optional<StoredShowcase> findLatest(long beforeShowcaseId) {
        return ofNullable(
            storedShowcaseRepository.readLatest(beforeShowcaseId)
        );
    }

    private Optional<StoredShowcase> findLatestByLikes(int minLikes, int maxLikes) {
        return ofNullable(
            storedShowcaseRepository.readLatestByLikes(minLikes, maxLikes)
        );
    }

    private Optional<StoredShowcase> findLatestByLikes(int minLikes, int maxLikes, long beforeShowcaseId) {
        return ofNullable(
            storedShowcaseRepository.readLatestByLikes(minLikes, maxLikes, beforeShowcaseId)
        );
    }

    public Optional<StoredShowcase> findNextByLikes(TalkContext context, TalkRequest request, Long lastShowcaseIdShown) {
        return findNextByLikes(
            lastShowcaseIdShown,
            () -> context.getInt(request.getChatId(), EXPLORE_BY_LIKES_LAST_LIKE_COUNT),
            lastLikeCount -> context.set(request.getChatId(), EXPLORE_BY_LIKES_LAST_LIKE_COUNT, lastLikeCount)
        );
    }

    public Optional<StoredShowcase> findNextByLikes(
        Long lastShowcaseIdShown,
        Supplier<Integer> lastLikeCountGetter,
        Consumer<Integer> lastLikeCountSetter
    ) {
        Integer minLikeCount = null;
        Integer lastLikeCount = lastShowcaseIdShown == null ?
            null :
            lastLikeCountGetter.get();
        if (lastLikeCount == null) {
            minLikeCount = 1;
            lastLikeCount = storedShowcaseRepository.readMaxLikes();
        }
        if (minLikeCount == null) {
            minLikeCount = lastLikeCount / 2;
        }

        Optional<StoredShowcase> storedShowcase;

        do {
            storedShowcase = lastShowcaseIdShown == null ?
                findLatestByLikes(minLikeCount, lastLikeCount + 1) :
                findLatestByLikes(minLikeCount, lastLikeCount + 1, lastShowcaseIdShown);
            minLikeCount /= 2;
        } while (! storedShowcase.isPresent() && minLikeCount > 0);

        if (storedShowcase.isPresent()) {
            lastLikeCountSetter.accept(storedShowcase.get().getLikes());
        }

        return storedShowcase;
    }

    public Optional<StoredShowcase> findLatestByTags(Set<String> tagsApplied) {
        return findAllLatestByTags(tagsApplied)
            .findFirst();
    }

    public Optional<StoredShowcase> findLatestByTags(Set<String> tagsApplied, long beforeShowcaseId) {
        return findAllLatestByTags(tagsApplied)
            .filter(showcase -> showcase.getId() < beforeShowcaseId)
            .findFirst();
    }

    private Stream<StoredShowcase> findAllLatestByTags(Set<String> tagsApplied) {
        List<String> tags = tagsApplied.stream().map(storesUtils::normalizeTag).collect(Collectors.toList());

        try (Dao.DaoConnection conn = storedShowcaseRepository.transactionalConn()) {
            return storedTagRepository.findAllByName(conn, tags).stream()
                .flatMap(tag -> storedBoxTagRepository.readByTagId(conn, tag.getId()).stream())
                .map(boxTag -> storedShowcaseRepository.readByBoxId(conn, boxTag.getBoxId()))
                .filter(Objects::nonNull)
                .sorted(reverseOrder(comparingLong(StoredShowcase::getId)));
        }
    }
}
