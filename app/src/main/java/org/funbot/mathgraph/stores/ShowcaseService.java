package org.funbot.mathgraph.stores;

import org.funbot.mathgraph.admin.User;
import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.Dao.DaoConnection;
import org.logicas.librerias.talks.engine.TalkContext;
import org.logicas.librerias.talks.engine.TalkRequest;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class ShowcaseService {

    private static String LIKE_STAR = "STARS";
    private static String LIKE_HEARTH = "HEARTHS";

    private static ShowcaseService instance = null;

    private ShowcaseGenerator showcaseGenerator;
    private ShowcaseCache showcaseCache;
    private StoredFunRepository storedFunRepository;
    private StoredBoxRepository storedBoxRepository;
    private StoredShowcaseRepository storedShowcaseRepository;
    private StoredTagRepository storedTagRepository;
    private StoredBoxTagRepository storedBoxTagRepository;
    private StoredLikeRepository storedLikeRepository;

    public static synchronized ShowcaseService getInstance(Dao dao) {
        if (instance == null) {
            instance = new ShowcaseService(dao);
        }
        return instance;
    }

    public ShowcaseService(Dao dao) {
        showcaseGenerator = ShowcaseGenerator.getInstance(dao);
        showcaseCache = ShowcaseCache.getInstance(dao);
        storedFunRepository = StoredFunRepository.getInstance(dao);
        storedBoxRepository = StoredBoxRepository.getInstance(dao);
        storedShowcaseRepository = StoredShowcaseRepository.getInstance(dao);
        storedTagRepository = StoredTagRepository.getInstance(dao);
        storedBoxTagRepository = StoredBoxTagRepository.getInstance(dao);
        storedLikeRepository = StoredLikeRepository.getInstance(dao);
    }

    // FIND SHOWCASED FUNS

    public Optional<StoredShowcase> findShowcase(long showcaseId) {
        return showcaseGenerator.findShowcase(showcaseId);
    }

    public Optional<StoredShowcase> findShowcaseByBoxId(long boxId) {
        return showcaseGenerator.findShowcaseByBoxId(boxId);
    }

    public Optional<StoredShowcase> findLatest() {
        return findLatest(null);
    }

    public Optional<StoredShowcase> findLatest(Long beforeShowcaseId) {
        Optional<StoredShowcase> cachedFun = showcaseCache.findLatest(beforeShowcaseId);
        if (cachedFun != null) { // cache is loaded
            return cachedFun;
        }

        return showcaseGenerator.findLatest(beforeShowcaseId);
    }

    public Optional<StoredShowcase> findNextByLikes(TalkContext context, TalkRequest request) {
        return findNextByLikes(context, request, null);
    }

    public Optional<StoredShowcase> findNextByLikes(TalkContext context, TalkRequest request, Long lastShowcaseIdShown) {
        Optional<StoredShowcase> cachedFun = showcaseCache.findNextByLikes(lastShowcaseIdShown);
        if (cachedFun != null) { // cache is loaded
            return cachedFun;
        }

        return showcaseGenerator.findNextByLikes(context, request, lastShowcaseIdShown);
    }

    public Optional<StoredShowcase> findLatestByTags(Set<String> tagsApplied) {
        return showcaseGenerator.findLatestByTags(tagsApplied);
    }

    public Optional<StoredShowcase> findLatestByTags(Set<String> tagsApplied, long beforeShowcaseId) {
        return showcaseGenerator.findLatestByTags(tagsApplied, beforeShowcaseId);
    }

    // SHOWCASE FUN

    public StoredShowcase showcaseFun(User user, Long boxId) {
        try (DaoConnection conn = storedShowcaseRepository.transactionalConn()) {
            LocalDateTime now = LocalDateTime.now();

            StoredBox storedBox = storedBoxRepository.readById(boxId);
            storedBox.setShowcasedAt(now);
            storedBox.setUpdatedAt(now);
            storedBoxRepository.save(storedBox);

            StoredShowcase storedShowcase = new StoredShowcase();
            storedShowcase.setBoxId(storedBox.getId());
            storedShowcase.setLikes(0);
            storedShowcase.setShowcasedByUserId(user.getId());
            storedShowcase.setCreatedAt(now);
            storedShowcaseRepository.save(storedShowcase);

            conn.commit();

            return storedShowcase;
        }
    }

    // LIKE

    public void like(User user, StoredShowcase showcase) {
        likeByStar(user, showcase);
    }

    private void likeByStar(User user, StoredShowcase showcase) {
        try (DaoConnection conn = storedLikeRepository.transactionalConn()) {
            LocalDateTime now = LocalDateTime.now();

            StoredLike like = new StoredLike();
            like.setType(LIKE_STAR);
            like.setUserId(user.getId());
            like.setShowcaseId(showcase.getId());
            like.setStrength(1);
            like.setCreatedAt(now);
            storedLikeRepository.save(like);

            showcase.setLikes(showcase.getLikes() + like.getStrength());
            storedShowcaseRepository.save(showcase);

            conn.commit();
        }
    }

    public Optional<StoredLike> findLike(User user, StoredShowcase showcase) {
        return storedLikeRepository.readByUserAndShowcase(user.getId(), showcase.getId());
    }

    // TAGS

    public List<StoredTag> findMostUsedTags() {
        try (DaoConnection conn = storedTagRepository.transactionalConn()) {
            return storedTagRepository.readByIds(conn, storedBoxTagRepository.readMostUsedTagIds(conn, 10));
        }
    }

//    public List<StoredTag> findMostUsedTags() {
//        return storedTagRepository.readByIds(findMostUsedTagIds());
//    }
//
//    private List<Long> findMostUsedTagIds() {
//        return storedBoxTagRepository.readAll().stream()
//            .collect(groupingBy(StoredBoxTag::getTagId))
//            .entrySet().stream()
//            .map(entry -> new SimpleEntry<>(entry.getKey(), entry.getValue().size()))
//            .sorted(reverseOrder(comparingInt(SimpleEntry::getValue)))
//            .limit(10)
//            .map(SimpleEntry::getKey)
//            .collect(Collectors.toList());
//    }
}
