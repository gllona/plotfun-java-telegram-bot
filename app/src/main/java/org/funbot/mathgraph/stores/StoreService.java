package org.funbot.mathgraph.stores;

import org.funbot.mathgraph.admin.User;
import org.funbot.mathgraph.plot.PlotStyle;
import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.Dao.DaoConnection;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class StoreService {

    private static StoreService instance = null;

    private StoredFunRepository storedFunRepository;
    private StoredBoxRepository storedBoxRepository;
    private StoredShowcaseRepository storedShowcaseRepository;
    private StoredTagRepository storedTagRepository;
    private StoredBoxTagRepository storedBoxTagRepository;
    private StoredLikeRepository storedLikeRepository;

    public static synchronized StoreService getInstance(Dao dao) {
        if (instance == null) {
            instance = new StoreService(dao);
        }
        return instance;
    }

    public StoreService(Dao dao) {
        storedFunRepository = StoredFunRepository.getInstance(dao);
        storedBoxRepository = StoredBoxRepository.getInstance(dao);
        storedShowcaseRepository = StoredShowcaseRepository.getInstance(dao);
        storedTagRepository = StoredTagRepository.getInstance(dao);
        storedBoxTagRepository = StoredBoxTagRepository.getInstance(dao);
        storedLikeRepository = StoredLikeRepository.getInstance(dao);
    }

    // FIND USER BOXES

    public List<StoredBox> getUserBoxes(User user) {
        return storedBoxRepository.readByUserId(user.getId()); // shallow
    }

    // FIND BOX AND FUN

    public Optional<StoredBox> findBox(long boxId) {
        return findBox(null, boxId);
    }

    public Optional<StoredBox> findBox(DaoConnection conn, long boxId) {
        StoredBox storedBox = storedBoxRepository.readById(conn, boxId);

        if (storedBox == null) {
            return Optional.empty();
        }

        StoredFun storedFun = storedFunRepository.readById(conn, storedBox.getFunId());
        storedBox.setFun(storedFun);

        List<StoredBoxTag> storedBoxTagRels = storedBoxTagRepository.readByBoxId(conn, storedBox.getId());
        List<Long> storedTagIds = storedBoxTagRels.stream().map(StoredBoxTag::getTagId).collect(Collectors.toList());
        List<StoredTag> storedTags = storedTagRepository.readByIds(conn, storedTagIds);
        storedBox.setTags(new HashSet<>(storedTags));

        return Optional.of(storedBox); // deep
    }

    // CREATE OR UPDATE FUN AND BOX IT

    public StoredBox saveFun(
        Long boxId, Long showcaseId,
        String name, List<String> tags,
        String type, String expressionX, String expressionY,
        String rangeStart, String rangeEnd,
        PlotStyle plotStyle, User user
    ) {
        if (boxId == null) {
            return createNewFun(showcaseId, name, tags, type, expressionX, expressionY, rangeStart, rangeEnd, plotStyle, user);
        } else {
            return updateFun(boxId, name, tags, type, expressionX, expressionY, rangeStart, rangeEnd, plotStyle, user);
        }
    }

    private StoredBox createNewFun(
        Long showcaseId,
        String name, List<String> tags,
        String type, String expressionX, String expressionY,
        String rangeStart, String rangeEnd,
        PlotStyle plotStyle,
        User user
    ) {
        try (DaoConnection conn = storedFunRepository.transactionalConn()) {
            StoredFun storedFun = createNewFun(conn, user, expressionX, expressionY, type);
            StoredBox storedBox = createNewBox(conn, user, storedFun, name, rangeStart, rangeEnd, plotStyle, showcaseId);

            Set<StoredTag> storedTags = findOrCreateTags(conn, user, tags);
            storedBox.setTags(storedTags);
            tagBox(conn, storedBox, storedTags);

            conn.commit();

            return storedBox;
        }
    }

    private StoredFun createNewFun(DaoConnection conn, User user, String expressionX, String expressionY, String type) {
        StoredFun storedFun = new StoredFun();
        storedFun.setType(type);
        storedFun.setExpressionX(expressionX);
        storedFun.setExpressionY(expressionY);
        storedFun.setCreatedByUserId(user.getId());
        storedFun.setCreatedAt(LocalDateTime.now());

        storedFunRepository.save(conn, storedFun);

        return storedFun;
    }

    private StoredBox createNewBox(
        DaoConnection conn,
        User user,
        StoredFun storedFun,
        String name,
        String rangeStart, String rangeEnd,
        PlotStyle plotStyle,
        Long showcaseId
    ) {
        StoredBox storedBox = new StoredBox();
        storedBox.setFunId(storedFun.getId());
        storedBox.setName(name);
        storedBox.setRangeStart(rangeStart);
        storedBox.setRangeEnd(rangeEnd);
        storedBox.setPlotStyle(plotStyle);
        storedBox.setCreatedByUserId(user.getId());
        storedBox.setCopiedFromShowcaseId(showcaseId);
        storedBox.setCreatedAt(LocalDateTime.now());

        storedBox.setFun(storedFun);

        storedBoxRepository.save(conn, storedBox);

        return storedBox;
    }

    private StoredBox updateFun(Long boxId, String name, List<String> tags, String type, String expressionX, String expressionY, String rangeStart, String rangeEnd, PlotStyle plotStyle, User user) {
        try (DaoConnection conn = storedFunRepository.transactionalConn()) {
            StoredBox storedBox = findBox(conn, boxId).get();
            StoredFun storedFun = storedBox.getFun();

            updateFun(conn, storedFun, expressionX, expressionY, type);
            updateBox(conn, storedBox, name, rangeStart, rangeEnd, plotStyle);
            updateTags(conn, user, storedBox, tags);

            conn.commit();

            return storedBox;
        }
    }

    private void updateFun(DaoConnection conn, StoredFun storedFun, String expressionX, String expressionY, String type) {
        storedFun.setType(type);
        storedFun.setExpressionX(expressionX);
        storedFun.setExpressionY(expressionY);
        storedFunRepository.save(conn, storedFun);
    }

    private void updateBox(DaoConnection conn, StoredBox storedBox, String name, String rangeStart, String rangeEnd, PlotStyle plotStyle) {
        storedBox.setName(name);
        storedBox.setRangeStart(rangeStart);
        storedBox.setRangeEnd(rangeEnd);
        storedBox.setPlotStyle(plotStyle);
        storedBoxRepository.save(conn, storedBox);
    }

    // FUN TAG

    private Set<StoredTag> findOrCreateTags(DaoConnection conn, User user, List<String> tags) {
        Set<StoredTag> currentStoredTags = findTags(conn, tags);
        Set<String> currentTags = currentStoredTags.stream().map(StoredTag::getTag).collect(Collectors.toSet());

        Set<String> tagsToCreate = new HashSet<>(tags);
        tagsToCreate.removeAll(currentTags);

        Set<StoredTag> allStoredTags = tagsToCreate.stream().map(tag -> createTag(conn, user, tag)).collect(Collectors.toSet());
        allStoredTags.addAll(currentStoredTags);

        return allStoredTags;
    }

    private Set<StoredTag> findTags(DaoConnection conn, List<String> tags) {
        List<StoredTag> storedTags = storedTagRepository.findAllByName(conn, tags);
        return new HashSet<>(storedTags);
    }

    private StoredTag createTag(DaoConnection conn, User user, String tag) {
        StoredTag storedTag = new StoredTag();
        storedTag.setTag(tag);
        storedTag.setCreatedByUserId(user.getId());
        storedTag.setCreatedAt(LocalDateTime.now());

        storedTagRepository.save(conn, storedTag);

        return storedTag;
    }

    private void updateTags(DaoConnection conn, User user, StoredBox storedBox, List<String> tags) {
        Set<StoredTag> storedTags = findOrCreateTags(conn, user, tags);

        Set<StoredTag> tagsToRemove = new HashSet<>(storedBox.getTags());
        tagsToRemove.removeAll(storedTags);

        Set<StoredTag> tagsToAdd = new HashSet<>(storedTags);
        tagsToAdd.removeAll(storedBox.getTags());

        untagBox(conn, storedBox, tagsToRemove);
        tagBox(conn, storedBox, tagsToAdd);
    }

    private void tagBox(DaoConnection conn, StoredBox storedBox, Set<StoredTag> storedTags) {
        for (StoredTag storedTag : storedTags) {
            StoredBoxTag storedBoxTag = new StoredBoxTag();
            storedBoxTag.setBoxId(storedBox.getId());
            storedBoxTag.setTagId(storedTag.getId());
            storedBoxTagRepository.save(conn, storedBoxTag);
        }
    }

    private void untagBox(DaoConnection conn, StoredBox storedBox, Set<StoredTag> storedTags) {
        for (StoredTag storedTag : storedTags) {
            StoredBoxTag storedBoxTag = storedBoxTagRepository.readByKeys(conn, storedBox.getId(), storedTag.getId());
            storedBoxTagRepository.delete(conn, storedBoxTag);
        }
    }

    // DELETE BOX

    public void delete(long boxId) {
        try (DaoConnection conn = storedFunRepository.transactionalConn()) {
            StoredBox storedBox = findBox(conn, boxId).get();

            untagBox(conn, storedBox, storedBox.getTags());
            storedBoxRepository.delete(conn, storedBox);
            storedFunRepository.delete(conn, storedBox.getFun());

            conn.commit();
        }
    }

    // SHOWCASES

    public Long getShowcaseId(long boxId) {
        return findBox(boxId)
            .filter(StoredBox::showcased)
            .map(box -> {
                StoredShowcase storedShowcase = storedShowcaseRepository.readByBoxId(null, box.getId());
                return storedShowcase == null ? null : storedShowcase.getId();
            })
            .orElse(null);
    }
}
