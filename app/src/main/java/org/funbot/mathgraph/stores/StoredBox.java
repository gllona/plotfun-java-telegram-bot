package org.funbot.mathgraph.stores;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.funbot.mathgraph.plot.PlotStyle;
import org.logicas.librerias.copersistance.entities.SingleKeyEntity;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class StoredBox extends SingleKeyEntity {

    public static final String FIELD_ID = "id";
    public static final String FIELD_FUN_ID = "fun_id";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_CREATED_BY_USER_ID = "created_by_user_id";
    public static final String FIELD_RANGE_START = "range_start";
    public static final String FIELD_RANGE_END = "range_end";
    public static final String FIELD_SHOW_AXES = "show_axes";
    public static final String FIELD_LINE_COLOR_RGB_CODE = "line_color_rgb_code";
    public static final String FIELD_THICKNESS = "thickness";
    public static final String FIELD_LINE_STYLE = "line_style";
    public static final String FIELD_BACKGROUND_COLOR_RGB_CODE = "background_color_rgb_code";
    public static final String FIELD_CREATED_AT = "created_at";
    public static final String FIELD_COPIED_FROM_SHOWCASE_ID = "copied_from_showcase_id";
    public static final String FIELD_SHOWCASED_AT = "showcased_at";
    public static final String FIELD_UPDATED_AT = "updated_at";

    public static final String[] FIELDS = {
        FIELD_ID,
        FIELD_FUN_ID,
        FIELD_NAME,
        FIELD_CREATED_BY_USER_ID,
        FIELD_RANGE_START,
        FIELD_RANGE_END,
        FIELD_SHOW_AXES,
        FIELD_LINE_COLOR_RGB_CODE,
        FIELD_THICKNESS,
        FIELD_LINE_STYLE,
        FIELD_BACKGROUND_COLOR_RGB_CODE,
        FIELD_CREATED_AT,
        FIELD_COPIED_FROM_SHOWCASE_ID,
        FIELD_SHOWCASED_AT,
        FIELD_UPDATED_AT
    };

    private static final String[] KEYS = {
        FIELD_ID
    };

    private Long id;
    private Long funId;
    private String name;
    private Long createdByUserId;
    private String rangeStart;
    private String rangeEnd;
    private Boolean showAxes;
    private String lineColorRgbCode;
    private Integer thickness;
    private String lineStyle;
    private String backgroundColorRgbCode;
    private LocalDateTime createdAt;
    private Long copiedFromShowcaseId;
    private LocalDateTime showcasedAt;
    private LocalDateTime updatedAt;

    private transient StoredFun fun;
    private transient Set<StoredTag> tags;

    @Override
    public Map<String, Object> parameters() {
        Map<String, Object> params = new HashMap<>();

        conditionalAddToParams(id, FIELD_ID, params);
        conditionalAddToParams(name, FIELD_NAME, params);
        conditionalAddToParams(funId, FIELD_FUN_ID, params);
        conditionalAddToParams(createdByUserId, FIELD_CREATED_BY_USER_ID, params);
        conditionalAddToParams(rangeStart, FIELD_RANGE_START, params);
        conditionalAddToParams(rangeEnd, FIELD_RANGE_END, params);
        conditionalAddToParams(showAxes, FIELD_SHOW_AXES, params);
        conditionalAddToParams(lineColorRgbCode, FIELD_LINE_COLOR_RGB_CODE, params);
        conditionalAddToParams(thickness, FIELD_THICKNESS, params);
        conditionalAddToParams(lineStyle, FIELD_LINE_STYLE, params);
        conditionalAddToParams(backgroundColorRgbCode, FIELD_BACKGROUND_COLOR_RGB_CODE, params);
        conditionalAddToParams(copiedFromShowcaseId, FIELD_COPIED_FROM_SHOWCASE_ID, params);
        conditionalAddToParams(showcasedAt, FIELD_SHOWCASED_AT, params);

        return params;
    }

    @Override
    public String[] getKeyColumns() {
        return KEYS;
    }

    public String getTagsDescription() {
        return tags == null ?
            null :
            tags.stream().map(StoredTag::getTag).sorted().collect(Collectors.joining(", "));
    }

    public boolean showcased() {
        return showcasedAt != null;
    }

    public PlotStyle getPlotStyle() {
        return PlotStyle.buildWithDefaultValues(
            getShowAxes(),
            getLineColorRgbCode(),
            getThickness(),
            getLineStyle(),
            getBackgroundColorRgbCode()
        );
    }

    public void setPlotStyle(PlotStyle plotStyle) {
        setShowAxes(plotStyle.isShowAxes());
        setLineColorRgbCode(plotStyle.getLineColorRgbCode());
        setThickness(plotStyle.getThickness());
        setLineStyle(plotStyle.getLineStyle().name());
        setBackgroundColorRgbCode(plotStyle.getBackgroundColorRgbCode());
    }
}
