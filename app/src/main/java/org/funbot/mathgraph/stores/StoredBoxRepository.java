package org.funbot.mathgraph.stores;

import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.repositories.SingleKeyEntityRepository;

import java.util.List;

public class StoredBoxRepository extends SingleKeyEntityRepository<StoredBox> {

    private static StoredBoxRepository instance;

    private Dao dao;

    private StoredBoxRepository(Dao dao) {
        this.dao = dao;
    }

    public static synchronized StoredBoxRepository getInstance(Dao dao) {
        if (instance == null) {
            instance = new StoredBoxRepository(dao);
        }
        return instance;
    }

    private static final String TABLE = "boxes";

    @Override
    protected Dao dao() {
        return dao;
    }

    @Override
    public String tableName() {
        return TABLE;
    }

    @Override
    protected String[] columnNames() {
        return StoredBox.FIELDS;
    }

    @Override
    public String idFieldName() {
        return StoredBox.FIELD_ID;
    }

    public List<StoredBox> readByUserId(long userId) {
        return readByField(StoredBox.FIELD_CREATED_BY_USER_ID, userId);
    }
}
