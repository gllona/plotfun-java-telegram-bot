package org.funbot.mathgraph.stores;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.logicas.librerias.copersistance.entities.DualKeyEntity;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class StoredBoxTag extends DualKeyEntity {

    public static final String FIELD_BOX_ID = "box_id";
    public static final String FIELD_TAG_ID = "tag_id";
    public static final String FIELD_CREATED_AT = "created_at";

    public static final String[] FIELDS = {
        FIELD_BOX_ID,
        FIELD_TAG_ID,
        FIELD_CREATED_AT
    };

    private static final String[] KEYS = {
        FIELD_BOX_ID,
        FIELD_TAG_ID
    };

    private Long boxId;
    private Long tagId;
    private LocalDateTime createdAt;

    @Override
    public Map<String, Object> parameters() {
        Map<String, Object> params = new HashMap<>();

        conditionalAddToParams(boxId, FIELD_BOX_ID, params);
        conditionalAddToParams(tagId, FIELD_TAG_ID, params);

        return params;
    }

    @Override
    public String[] getKeyColumns() {
        return KEYS;
    }

    @Override
    public Long getId1() {
        return boxId;
    }

    @Override
    public Long getId2() {
        return tagId;
    }

    @Override
    public void setIds(Long key1, Long key2) {
        boxId = key1;
        tagId = key2;
    }
}
