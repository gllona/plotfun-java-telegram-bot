package org.funbot.mathgraph.stores;

import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.repositories.DualKeyEntityRepository;

import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Comparator.comparingInt;
import static java.util.Comparator.reverseOrder;
import static java.util.stream.Collectors.groupingBy;
import static org.funbot.mathgraph.stores.StoredBoxTag.*;

public class StoredBoxTagRepository extends DualKeyEntityRepository<StoredBoxTag> {

    private static StoredBoxTagRepository instance;

    private Dao dao;

    private StoredBoxTagRepository(Dao dao) {
        this.dao = dao;
    }

    public static synchronized StoredBoxTagRepository getInstance(Dao dao) {
        if (instance == null) {
            instance = new StoredBoxTagRepository(dao);
        }
        return instance;
    }

    private static final String TABLE = "boxes_tags";

    @Override
    protected Dao dao() {
        return dao;
    }

    @Override
    public String tableName() {
        return TABLE;
    }

    @Override
    protected String[] columnNames() {
        return FIELDS;
    }

    @Override
    protected String id1FieldName() {
        return FIELD_BOX_ID;
    }

    @Override
    protected String id2FieldName() {
        return FIELD_TAG_ID;
    }

    public StoredBoxTag readByKeys(Dao.DaoConnection conn, Long boxId, Long tagId) {
        List<StoredBoxTag> storedBoxesTags = readByCondition(conn, "box_id = " + boxId + " AND tag_id = " + tagId);
        return storedBoxesTags.isEmpty() ? null : storedBoxesTags.get(0);
    }

    public List<StoredBoxTag> readByBoxId(Dao.DaoConnection conn, Long boxId) {
        return readByField(conn, FIELD_BOX_ID, boxId);
    }

    public List<StoredBoxTag> readByTagId(Dao.DaoConnection conn, Long tagId) {
        return readByField(conn, FIELD_TAG_ID, tagId);
    }

    public List<Long> readMostUsedTagIds(Dao.DaoConnection conn, int count) {
        List<LongAggregationResult> countsByTag = aggregateLongGroupBy(
            conn,
            Aggregation.COUNT, FIELD_BOX_ID,
            null,
            FIELD_TAG_ID,
            null,
            "COUNT(" + FIELD_BOX_ID + ")", "DESC",
            count, 0
        );
        return countsByTag.stream().map(result -> result.groupByValue).collect(Collectors.toList());
    }
}
