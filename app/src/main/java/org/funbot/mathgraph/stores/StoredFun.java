package org.funbot.mathgraph.stores;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.logicas.librerias.copersistance.entities.SingleKeyEntity;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class StoredFun extends SingleKeyEntity {

    public static final String FIELD_ID = "id";
    public static final String FIELD_CREATED_BY_USER_ID = "created_by_user_id";
    public static final String FIELD_TYPE = "type";
    public static final String FIELD_EXPRESSION_X = "expression_x";
    public static final String FIELD_EXPRESSION_Y = "expression_y";
    public static final String FIELD_CREATED_AT = "created_at";
    public static final String FIELD_UPDATED_AT = "updated_at";

    public static final String[] FIELDS = {
        FIELD_ID,
        FIELD_CREATED_BY_USER_ID,
        FIELD_TYPE,
        FIELD_EXPRESSION_X,
        FIELD_EXPRESSION_Y,
        FIELD_CREATED_AT,
        FIELD_UPDATED_AT
    };

    private static final String[] KEYS = {
        FIELD_ID
    };

    private Long id;
    private Long createdByUserId;
    private String type;
    private String expressionX;
    private String expressionY;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    @Override
    public Map<String, Object> parameters() {
        Map<String, Object> params = new HashMap<>();

        conditionalAddToParams(id, FIELD_ID, params);
        conditionalAddToParams(createdByUserId, FIELD_CREATED_BY_USER_ID, params);
        conditionalAddToParams(type, FIELD_TYPE, params);
        conditionalAddToParams(expressionX, FIELD_EXPRESSION_X, params);
        conditionalAddToParams(expressionY, FIELD_EXPRESSION_Y, params);

        return params;
    }

    @Override
    public String[] getKeyColumns() {
        return KEYS;
    }
}
