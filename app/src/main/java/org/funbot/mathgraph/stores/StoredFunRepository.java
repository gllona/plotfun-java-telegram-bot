package org.funbot.mathgraph.stores;

import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.repositories.SingleKeyEntityRepository;

public class StoredFunRepository extends SingleKeyEntityRepository<StoredFun> {

    private static StoredFunRepository instance;

    private Dao dao;

    private StoredFunRepository(Dao dao) {
        this.dao = dao;
    }

    public static synchronized StoredFunRepository getInstance(Dao dao) {
        if (instance == null) {
            instance = new StoredFunRepository(dao);
        }
        return instance;
    }

    private static final String TABLE = "funs";

    @Override
    protected Dao dao() {
        return dao;
    }

    @Override
    public String tableName() {
        return TABLE;
    }

    @Override
    protected String[] columnNames() {
        return StoredFun.FIELDS;
    }

    @Override
    public String idFieldName() {
        return StoredFun.FIELD_ID;
    }
}
