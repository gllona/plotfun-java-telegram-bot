package org.funbot.mathgraph.stores;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.logicas.librerias.copersistance.entities.SingleKeyEntity;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class StoredLike extends SingleKeyEntity {

    public static final String FIELD_ID = "id";
    public static final String FIELD_USER_ID = "user_id";
    public static final String FIELD_SHOWCASE_ID = "showcase_id";
    public static final String FIELD_TYPE = "type";
    public static final String FIELD_STRENGTH = "strength";
    public static final String FIELD_CREATED_AT = "created_at";

    public static final String[] FIELDS = {
        FIELD_ID,
        FIELD_USER_ID,
        FIELD_SHOWCASE_ID,
        FIELD_TYPE,
        FIELD_STRENGTH,
        FIELD_CREATED_AT
    };

    private static final String[] KEYS = {
        FIELD_ID
    };

    private Long id;
    private Long userId;
    private Long showcaseId;
    private String type;
    private Integer strength;
    private LocalDateTime createdAt;

    @Override
    public Map<String, Object> parameters() {
        Map<String, Object> params = new HashMap<>();

        conditionalAddToParams(id, FIELD_ID, params);
        conditionalAddToParams(userId, FIELD_USER_ID, params);
        conditionalAddToParams(showcaseId, FIELD_SHOWCASE_ID, params);
        conditionalAddToParams(type, FIELD_TYPE, params);
        conditionalAddToParams(strength, FIELD_STRENGTH, params);

        return params;
    }

    @Override
    public String[] getKeyColumns() {
        return KEYS;
    }
}
