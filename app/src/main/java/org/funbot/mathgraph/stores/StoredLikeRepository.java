package org.funbot.mathgraph.stores;

import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.repositories.SingleKeyEntityRepository;

import java.util.List;
import java.util.Optional;

public class StoredLikeRepository extends SingleKeyEntityRepository<StoredLike> {

    private static StoredLikeRepository instance;

    private Dao dao;

    private StoredLikeRepository(Dao dao) {
        this.dao = dao;
    }

    public static synchronized StoredLikeRepository getInstance(Dao dao) {
        if (instance == null) {
            instance = new StoredLikeRepository(dao);
        }
        return instance;
    }

    private static final String TABLE = "likes";

    @Override
    protected Dao dao() {
        return dao;
    }

    @Override
    public String tableName() {
        return TABLE;
    }

    @Override
    protected String[] columnNames() {
        return StoredLike.FIELDS;
    }

    @Override
    public String idFieldName() {
        return StoredLike.FIELD_ID;
    }

    public Optional<StoredLike> readByUserAndShowcase(long userId, long showcaseId) {
        List<StoredLike> storedLikes = readByCondition(
            StoredLike.FIELD_USER_ID + " = " + userId + " AND " +
                StoredLike.FIELD_SHOWCASE_ID + " = " + showcaseId
        );
        return storedLikes.size() > 0 ?
            Optional.of(storedLikes.get(0)) :
            Optional.empty();
    }
}
