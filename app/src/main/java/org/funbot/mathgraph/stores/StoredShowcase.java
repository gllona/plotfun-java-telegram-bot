package org.funbot.mathgraph.stores;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.logicas.librerias.copersistance.entities.SingleKeyEntity;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class StoredShowcase extends SingleKeyEntity {

    public static final String FIELD_ID = "id";
    public static final String FIELD_BOX_ID = "box_id";
    public static final String FIELD_LIKES = "likes";
    public static final String FIELD_SHOWCASED_BY_USER_ID = "showcased_by_user_id";
    public static final String FIELD_CREATED_AT = "created_at";

    public static final String[] FIELDS = {
        FIELD_ID,
        FIELD_BOX_ID,
        FIELD_LIKES,
        FIELD_SHOWCASED_BY_USER_ID,
        FIELD_CREATED_AT
    };

    private static final String[] KEYS = {
        FIELD_ID
    };

    private Long id;
    private Long boxId;
    private Integer likes;
    private Long showcasedByUserId;
    private LocalDateTime createdAt;

    @Override
    public Map<String, Object> parameters() {
        Map<String, Object> params = new HashMap<>();

        conditionalAddToParams(id, FIELD_ID, params);
        conditionalAddToParams(boxId, FIELD_BOX_ID, params);
        conditionalAddToParams(likes, FIELD_LIKES, params);
        conditionalAddToParams(showcasedByUserId, FIELD_SHOWCASED_BY_USER_ID, params);

        return params;
    }

    @Override
    public String[] getKeyColumns() {
        return KEYS;
    }
}
