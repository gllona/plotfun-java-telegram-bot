package org.funbot.mathgraph.stores;

import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.repositories.SingleKeyEntityRepository;

import java.util.List;

import static org.funbot.mathgraph.stores.StoredShowcase.*;

public class StoredShowcaseRepository extends SingleKeyEntityRepository<StoredShowcase> {

    private static StoredShowcaseRepository instance;

    private Dao dao;

    private StoredShowcaseRepository(Dao dao) {
        this.dao = dao;
    }

    public static synchronized StoredShowcaseRepository getInstance(Dao dao) {
        if (instance == null) {
            instance = new StoredShowcaseRepository(dao);
        }
        return instance;
    }

    private static final String TABLE = "showcases";

    @Override
    protected Dao dao() {
        return dao;
    }

    @Override
    public String tableName() {
        return TABLE;
    }

    @Override
    protected String[] columnNames() {
        return FIELDS;
    }

    @Override
    public String idFieldName() {
        return FIELD_ID;
    }

    public StoredShowcase readLatest() {
        List<StoredShowcase> storedShowcases = readAllByPage(
            1, 1,
            FIELD_ID, OrderBy.DESC);
        return storedShowcases.isEmpty() ? null : storedShowcases.get(0);
    }

    public StoredShowcase readByBoxId(Dao.DaoConnection conn, Long boxId) {
        List<StoredShowcase> storedShowcases = readByField(conn, FIELD_BOX_ID, boxId);
        return storedShowcases.isEmpty() ? null : storedShowcases.get(0);
    }

    public StoredShowcase readLatest(long beforeShowcaseId) {
        List<StoredShowcase> storedShowcases = readAllByPage(
            1, 1,
            FIELD_ID, OrderBy.DESC,
            FIELD_ID + " < " + beforeShowcaseId);
        return storedShowcases.isEmpty() ? null : storedShowcases.get(0);
    }

    public StoredShowcase readLatestByLikes(int minLikes, int maxLikes) {
        List<StoredShowcase> storedShowcases = readAllByPage(
            1, 1,
            FIELD_LIKES, OrderBy.DESC,
            FIELD_LIKES + ">=" +  minLikes + " AND " + FIELD_LIKES + "<" + maxLikes);
        return storedShowcases.isEmpty() ? null : storedShowcases.get(0);
    }

    public StoredShowcase readLatestByLikes(int minLikes, int maxLikes, long beforeShowcaseId) {
        List<StoredShowcase> storedShowcases = readAllByPage(
            1, 1,
            FIELD_ID, OrderBy.DESC,
            FIELD_LIKES + ">=" +  minLikes + " AND " + FIELD_LIKES + "<" + maxLikes +
                " AND " + FIELD_ID + " < " + beforeShowcaseId);
        return storedShowcases.isEmpty() ? null : storedShowcases.get(0);
    }

    public int readMaxLikes() {
        return aggregate(Aggregation.MAX, FIELD_LIKES, null);
    }
}
