package org.funbot.mathgraph.stores;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.logicas.librerias.copersistance.entities.SingleKeyEntity;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class StoredTag extends SingleKeyEntity {

    public static final String FIELD_ID = "id";
    public static final String FIELD_TAG = "tag";
    public static final String FIELD_CREATED_BY_USER_ID = "created_by_user_id";
    public static final String FIELD_CREATED_AT = "created_at";

    public static final String[] FIELDS = {
        FIELD_ID,
        FIELD_TAG,
        FIELD_CREATED_BY_USER_ID,
        FIELD_CREATED_AT
    };

    private static final String[] KEYS = {
        FIELD_ID
    };

    private Long id;
    private String tag;
    private Long createdByUserId;
    private LocalDateTime createdAt;

    @Override
    public Map<String, Object> parameters() {
        Map<String, Object> params = new HashMap<>();

        conditionalAddToParams(id, FIELD_ID, params);
        conditionalAddToParams(tag, FIELD_TAG, params);
        conditionalAddToParams(createdByUserId, FIELD_CREATED_BY_USER_ID, params);

        return params;
    }

    @Override
    public String[] getKeyColumns() {
        return KEYS;
    }
}
