package org.funbot.mathgraph.stores;

import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.Dao.DaoConnection;
import org.logicas.librerias.copersistance.repositories.QueryBuilder;
import org.logicas.librerias.copersistance.repositories.SingleKeyEntityRepository;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.*;

public class StoredTagRepository extends SingleKeyEntityRepository<StoredTag> {

    private static StoredTagRepository instance;

    private Dao dao;

    private StoredTagRepository(Dao dao) {
        this.dao = dao;
    }

    public static synchronized StoredTagRepository getInstance(Dao dao) {
        if (instance == null) {
            instance = new StoredTagRepository(dao);
        }
        return instance;
    }

    private static final String TABLE = "tags";

    @Override
    protected Dao dao() {
        return dao;
    }

    @Override
    public String tableName() {
        return TABLE;
    }

    @Override
    protected String[] columnNames() {
        return StoredTag.FIELDS;
    }

    @Override
    public String idFieldName() {
        return StoredTag.FIELD_ID;
    }

    public List<StoredTag> findAllByName(DaoConnection conn, List<String> tags) {
        if (tags.isEmpty()) {
            return emptyList();
        }

        String condition = "tag IN ('" +
            tags.stream().map(QueryBuilder::escapeSql).collect(Collectors.joining("', '")) +
            "')";
        return readByCondition(conn, condition);
    }
}
