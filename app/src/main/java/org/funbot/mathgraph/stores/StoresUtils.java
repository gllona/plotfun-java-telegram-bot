package org.funbot.mathgraph.stores;

import org.funbot.mathgraph.plot.PlotStyle;
import org.logicas.librerias.talks.engine.TalkContext;
import org.logicas.librerias.talks.engine.TalkRequest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;
import java.util.stream.Collectors;

public class StoresUtils {

    public static final String STORE_BOX_ID = "STORE_ID";
    public static final String STORE_TYPE = "STORE_TYPE";
    public static final String STORE_EXPRESSION_X = "STORE_EXPRESSION_X";
    public static final String STORE_EXPRESSION_Y = "STORE_EXPRESSION_Y";
    public static final String STORE_RANGE_START = "STORE_RANGE_START";
    public static final String STORE_RANGE_END = "STORE_RANGE_END";
    public static final String STORE_NAME = "STORE_NAME";
    public static final String STORE_TAGS = "STORE_TAGS";
    public static final String STORE_DISPLAY_AXES = "STORE_DISPLAY_AXES";
    public static final String STORE_LINE_COLOR = "STORE_LINE_COLOR";
    public static final String STORE_LINE_THICKNESS = "STORE_LINE_THICKNESS";
    public static final String STORE_LINE_STYLE = "STORE_LINE_STYLE";
    public static final String STORE_BACKGROUND_COLOR = "STORE_BACKGROUND_COLOR";
    public static final String STORE_SHOWCASE_ID = "STORE_SHOWCASE_ID";

    private static StoresUtils instance = null;

    public static synchronized StoresUtils getInstance() {
        if (instance == null) {
            instance = new StoresUtils();
        }
        return instance;
    }

    public void saveInContext(
        TalkContext context,
        TalkRequest request,
        Long boxId,
        String name, String tags,
        String type, String expressionX, String expressionY, String rangeStart, String rangeEnd,
        PlotStyle plotStyle
    ) {
        context.set(request.getChatId(), STORE_BOX_ID, boxId);
        context.set(request.getChatId(), STORE_NAME, name);
        context.set(request.getChatId(), STORE_TAGS, tags);
        context.set(request.getChatId(), STORE_TYPE, type);
        context.set(request.getChatId(), STORE_EXPRESSION_X, expressionX);
        context.set(request.getChatId(), STORE_EXPRESSION_Y, expressionY);
        context.set(request.getChatId(), STORE_RANGE_START, rangeStart);
        context.set(request.getChatId(), STORE_RANGE_END, rangeEnd);

        PlotStyle.saveInStoresContext(context, request.getChatId(), plotStyle);
    }

    public void clearContext(TalkContext context, TalkRequest request) {
        context.remove(request.getChatId(), STORE_BOX_ID);
        context.remove(request.getChatId(), STORE_SHOWCASE_ID);
        context.remove(request.getChatId(), STORE_NAME);
        context.remove(request.getChatId(), STORE_TAGS);
        context.remove(request.getChatId(), STORE_TYPE);
        context.remove(request.getChatId(), STORE_EXPRESSION_X);
        context.remove(request.getChatId(), STORE_EXPRESSION_Y);
        context.remove(request.getChatId(), STORE_RANGE_START);
        context.remove(request.getChatId(), STORE_RANGE_END);

        PlotStyle.clearStoresContext(context, request.getChatId());
    }

    public String extractTrailingHashId(String command) {
        return command.substring(command.lastIndexOf("_") + 1);
    }

    public String hashId(Long id) {
        return id.toString();
    }

    public Long unhashId(String hashId) {
        return Long.valueOf(hashId);
    }

    public String fullBoxDescription(StoredBox box) {
        StoredFun fun = box.getFun();
        return box.getName() +
            "\n" +
            "\nType: " + fun.getType() +
            "\n" + funExpressionDescription(fun) +
            "\n" +
            "\nRange: " + box.getRangeStart() + " .. " + box.getRangeEnd() +
            "\n" +
            "\nTags: " + boxTagsDescription(box) +
            "\n" +
            "\nCreated: " + formatDate(fun.getCreatedAt()) +
            "\nUpdated: " + formatDate(fun.getUpdatedAt()) +
            (box.showcased() ? "\nShowcased: " + formatDate(box.getShowcasedAt()) : "");
    }

    private String funExpressionDescription(StoredFun fun) {
        if (fun.getType().equals("XY")) {
            return "y = " + fun.getExpressionY();
        } else { // XYP
            return "x = " + fun.getExpressionX() +
                "\n" + "y = " + fun.getExpressionY();
        }
    }

    private String boxTagsDescription(StoredBox box) {
        Set<StoredTag> tags = box.getTags();
        return tags.isEmpty() ?
            "No tags" :
            tags.stream().map(StoredTag::getTag).sorted().collect(Collectors.joining(", "));
    }

    public String normalizeTag(String tag) {
        return tag.toLowerCase().trim()
            .replaceAll("-", "_")
            .replaceAll("  *", "_")
            .replaceAll("__+", "_");
    }

    private String formatDate(LocalDateTime moment) {
        return moment.format(DateTimeFormatter.ISO_LOCAL_DATE);
    }
}
