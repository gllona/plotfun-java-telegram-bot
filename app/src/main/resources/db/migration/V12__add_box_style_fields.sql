ALTER TABLE boxes
    ADD COLUMN show_axes TINYINT NULL DEFAULT NULL AFTER range_end,
    ADD COLUMN line_color_rgb_code VARCHAR(7) NULL DEFAULT NULL AFTER show_axes,
    ADD COLUMN thickness INT NULL DEFAULT NULL AFTER line_color_rgb_code,
    ADD COLUMN line_style VARCHAR(32) NULL DEFAULT NULL AFTER thickness,
    ADD COLUMN background_color_rgb_code VARCHAR(7) NULL DEFAULT NULL AFTER line_style;
