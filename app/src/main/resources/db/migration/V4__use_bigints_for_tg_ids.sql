ALTER TABLE talk_contexts
    MODIFY COLUMN session_id BIGINT NOT NULL;
