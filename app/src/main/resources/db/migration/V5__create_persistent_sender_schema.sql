CREATE TABLE telegram_message_sender_queue (
	id BIGINT NOT NULL AUTO_INCREMENT,
    talk_response TEXT NOT NULL,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;
