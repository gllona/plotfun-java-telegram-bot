CREATE TABLE boxes (
    id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) DEFAULT NULL,
    created_by_user_id BIGINT NOT NULL,
    type VARCHAR(20) NOT NULL,
    expression_x VARCHAR(255),
    expression_y VARCHAR(255),
    range_start VARCHAR(255),
    range_end VARCHAR(255),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    copied_from_showcase_id BIGINT DEFAULT NULL,
    showcased_at TIMESTAMP NULL DEFAULT NULL,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (created_by_user_id) REFERENCES users(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

CREATE INDEX boxes_created_at ON boxes (created_at);
CREATE INDEX boxes_showcased_at ON boxes (showcased_at);

CREATE TABLE tags (
    id BIGINT NOT NULL AUTO_INCREMENT,
    tag VARCHAR(255) NOT NULL,
    created_by_user_id BIGINT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (created_by_user_id) REFERENCES users(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

CREATE TABLE boxes_tags (
    id BIGINT NOT NULL AUTO_INCREMENT,
    box_id BIGINT NOT NULL,
    tag_id BIGINT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

CREATE TABLE showcases (
    id BIGINT NOT NULL AUTO_INCREMENT,
    box_id BIGINT NOT NULL,
    likes INT NOT NULL DEFAULT 0,
    showcased_by_user_id BIGINT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (box_id) REFERENCES boxes(id),
    FOREIGN KEY (showcased_by_user_id) REFERENCES users(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

CREATE INDEX showcases_created_at ON showcases (created_at);

CREATE TABLE likes (
    id BIGINT NOT NULL AUTO_INCREMENT,
    user_id BIGINT NOT NULL,
    showcase_id BIGINT NOT NULL,
    type ENUM('STARS', 'HEARTHS') NOT NULL,
    strength INT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (showcase_id) REFERENCES showcases(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

CREATE INDEX likes_type_strength ON likes (type, strength);
CREATE INDEX likes_created_at ON likes (created_at);
