CREATE TABLE funs (
    id BIGINT NOT NULL AUTO_INCREMENT,
    created_by_user_id BIGINT NOT NULL,
    type VARCHAR(20) NOT NULL,
    expression_x VARCHAR(255),
    expression_y VARCHAR(255),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (created_by_user_id) REFERENCES users(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

CREATE INDEX funs_created_at ON funs (created_at);

ALTER TABLE boxes
    DROP COLUMN type,
    DROP COLUMN expression_x,
    DROP COLUMN expression_y;

ALTER TABLE boxes
    ADD COLUMN fun_id BIGINT NOT NULL AFTER id,
    ADD FOREIGN KEY (fun_id) REFERENCES funs(id);
