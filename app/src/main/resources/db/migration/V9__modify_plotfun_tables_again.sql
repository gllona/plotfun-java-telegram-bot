ALTER TABLE boxes_tags
    DROP PRIMARY KEY,
    DROP COLUMN id,
    ADD PRIMARY KEY (box_id, tag_id);
